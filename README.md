# GO Service

## Service Description

## Migration Rules

- Migration file name must follow this format: [YYYYMMDD]_[DatabaseName]_[3DigitIncrementNumber].sql
- When deploy, always notify SRE team and Tech Lead if there is a new migration file to run
- Lead should manage migration file

## Used Database

## Used Table - Privileges

## Public path

## Coding Rules

- All features or bug fixes must be tested by one or more specs (unit-tests)
- All public API methods must be documented

## Naming Convention

- Always sort alphabetically all struct attributes
  - Have exception on struct for database table, id should be always on very top, pagination fields (limit, page, sortby) should be always on the bottom before timestamps and timestamps (created_at, updated_at and deleted_at), should be always on very bottom. Keep sorting among pagination fields and among timestamp fields.
- Always sort public function first
- Struct and interface should have first letter capitalized, ex: UserData
- Use camelCase for variable name, ex: messageData
- Use snake_case for json or database attribute, ex: message_data
- Public function use PascalCase Naming Convention, ex: RunProcessingForUser
- Private function use camelCase Naming Convention ex: peekIntoDatabase
- For import, seperate inti 3 section, go library, shipperid, and last but least other community library

  - Ex:

    ```
    "context"

    "bitbucket.org/shipperid/sdk-go-boilerplate"

    "github.com/someone/cool-shark"
    ```

## Deployment Rules

- Tech Lead should branch from master into release history branch
- Tech Lead/QA should create a new tag before release
- QA should tested from develop and master for each image docker

## Commit Rules

- Always create new branch for feature branch from card number, ex: PWMS-10001
  - Create the branch from jira card
  - Select Branch Type either Bug Fix or Feature
  - Branch name should from card number, ex: PWMS-10001
- Commit Message must follow this format: **<type>(<scope>): <subject>**, ex: feat(Rest): add new feature to be able to read mind
  - If resolving conflict, the message should be "resolve merge conflict"
- After task complete and status on Code Review, must **Pull Request** to deploy branch and assign **Tech Lead** as reviewer
  - If urgently need, please notify the Tech Lead by Personal Chat and put **[Urgent]** on Pull Request Name
  - Accepted and Merge will be handle by Tech Lead
  - If Tech Lead decline, decline must put comment what needed to fix or add or improve, and notify by Personal Chat
  - Tech Lead shouln't delete the feature branch, feature branch deleted after merged to Master
  - Tech Lead should rebase and merge on pull request, but create a commit message when merge to release or release candidate

### Commit Message Guideline

**Type**

- build: Changes that affect the build system or external dependencies (example scopes: Gomod, CMD)
- docs: Documentation only changes
- feat: A new feature
- fix: A bug fix
- perf: A code change that improves performance
- refactor: A code change that fixing code convention or naming convention
- style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- test: Adding missing tests or correcting existing tests

##### Update Email template in AWS SES

- Create Template

```
aws ses create-template --cli-input-json file://authenticationfailurewiththirdparty.json
```

- Delete Template

```
aws ses delete-template --template-name AuthenticationFailureWithThirdParty
```

##### Scope

Scope must be follow the Framework or Flow that used:

- Rest
- Use-Case
- Domain
- Entity
- Docs
- Test
- Gomod
- CMD
- Config

**Subject**  
The subject contains a succinct description of the change:

- Use the imperative, present tense: "change" not "changed" nor "changes"
- Don't capitalize the first letter
- No dot (.) at the end
