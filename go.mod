module bitbucket.org/shipperid/sdk-go-boilerplate

go 1.12

require (
	bitbucket.org/shipperid/driversvc v1.0.1
	bitbucket.org/shipperid/sdk-go v0.3.127
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/go-openapi/spec v0.19.2 // indirect
	github.com/go-openapi/swag v0.19.5 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gocql/gocql v0.0.0-20200624222514-34081eda590e
	github.com/golang/mock v1.4.3
	github.com/google/gops v0.3.6
	github.com/google/uuid v1.1.1
	github.com/nouney/randomstring v0.0.0-20180330205616-1374daa59f01
	github.com/smartystreets/goconvey v1.6.4
	github.com/swaggo/swag v1.6.3
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	github.com/xdg/stringprep v1.0.0 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
)
