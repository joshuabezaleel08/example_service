NAMESPACE = `echo sdk-go-boilerplate`
BUILD_TIME = `date +%FT%T%z`
BUILD_VERSION = `git describe --tag`
COMMIT_HASH = `git rev-parse --short HEAD`

.PHONY: build
build:
	@`go env GOPATH`/bin/swag init -g ./src/cmd/main.go && \
	go build -tags static -ldflags "-X main.Namespace=${NAMESPACE} \
		-X main.BuildTime=${BUILD_TIME} \
		-X main.BuildVersion=${BUILD_VERSION} \
		-X main.CommitHash=${COMMIT_HASH}" \
		-race -o ./build/app ./src/cmd

.PHONY: build-alpine
build-alpine:
	@go mod tidy && \
	go build -tags static -ldflags "-X main.Namespace=${NAMESPACE} \
	    -X main.BuildTime=${BUILD_TIME} \
	    -X main.BuildVersion=${BUILD_VERSION} \
	    -X main.CommitHash=${COMMIT_HASH}" \
		-o ./build/app ./src/cmd

.PHONY: kill-process
kill-process:
	@lsof -i :8080 | awk '$$1 ~ /app/ { print $$2 }' | xargs kill -9 || true

.PHONY: run
run: kill-process run-tests build
	@./build/app

.PHONY: debug
debug: kill-process build-debug
	@./build/app

.PHONY: swag-install
swag-install:
	@go get github.com/swaggo/swag/cmd/swag

.PHONY: integ-test
integ-test:
	@go clean -testcache ./...
	@go test -v --tags=integration -timeout 30s ./src/handler/rest -run ^TestRest$ 

.PHONY: run-tests
run-tests:
	@go test `go list ./... | grep -i 'domain\|usecase'` -cover

.PHONY: swaggo
swaggo:
	@/bin/rm -f ./docs/docs.go
	@`go env GOPATH`/bin/swag init -g ./src/cmd/main.go

.PHONY: build-debug
build-debug:
	@go build -gcflags "all=-N -l" \
		-o ./build/app ./src/cmd

# use by make attach pid=#PID
.PHONY: attach
attach:
	@dlv attach --headless \
		--listen=:2345 $(pid) \
		`go env GOPATH`/src/bitbucket.org/shipperid/omnichannelsvc \
		--log --api-version=2

# use by make mock source=#DOMAIN_NAME
.PHONY: mock
mock:
	@`go env GOPATH`/bin/mockgen -source src/business/domain/$(source)/$(source).go -destination src/business/domain/mock/$(source)/$(source).go

# use by make tag name=#TAG_NAME
.PHONY: tag
tag:
	@git tag $(name)
	@git push origin --tag
