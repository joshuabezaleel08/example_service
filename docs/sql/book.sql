DROP DATABASE IF EXISTS `book_db`;
CREATE DATABASE IF NOT EXISTS `book_db` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

DROP TABLE IF EXISTS `book_db`.`book`;
CREATE TABLE IF NOT EXISTS `book_db`.`book`(
    id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    partition_key INT NOT NULL DEFAULT 1 COMMENT "",
    title VARCHAR(128) NOT NULL COMMENT "",
    genre VARCHAR(32) NOT NULL COMMENT "",
    author_id BIGINT UNSIGNED NOT NULL,
    is_deleted BOOLEAN NOT NULL DEFAULT FALSE COMMENT "",
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP NULL,
    PRIMARY KEY (id)
) ENGINE=INNODB;

INSERT INTO `book_db`.`book` (id, title, genre, author_id)
    VALUES (1, "BookTitle1", "Genre 1", 1), (2, "BookTitle2", "Biography", 2), (3, "BookTitle3", "Genre 1", 1);

DROP TABLE IF EXISTS `book_db`.`author`;
CREATE TABLE IF NOT EXISTS `book_db`.`author`(
    id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL COMMENT "",
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP NULL,
    PRIMARY KEY (id)
) ENGINE=INNODB;

INSERT INTO `book_db`.`author` (id, name)
    VALUES (1, "Author Name 1"), (2, "Author Name 2");

DROP TABLE IF EXISTS `book_db`.`book_author`;
CREATE TABLE IF NOT EXISTS `book_db`.`book_author`(
    book_id BIGINT UNSIGNED,
    author_id BIGINT UNSIGNED
) ENGINE=INNODB;

INSERT INTO `book_db`.`book_author` (book_id, author_id)
    VALUES (1,1),(2,2),(3,1);