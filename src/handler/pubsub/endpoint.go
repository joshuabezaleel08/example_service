package pubsub

import (
	"bitbucket.org/shipperid/sdk-go/stdlib/kafkalib"
)

func (p *pubsub) HandleFoo(w kafkalib.ResponseWriter, r *kafkalib.Request) {
	// 	w.Commit(kafkalib.StatusOK)
}

func (p *pubsub) HandleBar(w kafkalib.ResponseWriter, r *kafkalib.Request) {
	// 	w.Commit(kafkalib.StatusOK)
}

func (p *pubsub) HandleOther(w kafkalib.ResponseWriter, r *kafkalib.Request) {
	// 	w.Commit(kafkalib.StatusOK)
}
