package pubsub

import (
	"sync"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/usecase"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"

	"bitbucket.org/shipperid/sdk-go/stdlib/kafkalib"
	"bitbucket.org/shipperid/sdk-go/stdlib/parser"
)

var (
	once = &sync.Once{}
)

type PubSub interface{}

type pubsub struct {
	logger   log.Logger
	opt      Options
	json     parser.JSONParser
	xml      parser.XMLParser
	consumer kafkalib.Consumer
	uc       *usecase.Usecase
}

type Options struct{}

func Init(logger log.Logger, opt Options, parse parser.Parser, consumer kafkalib.Consumer, uc *usecase.Usecase) PubSub {
	var ps *pubsub
	if consumer == nil {
		return nil
	}
	once.Do(func() {
		ps = &pubsub{
			logger:   logger,
			opt:      opt,
			json:     parse.JSONParser(),
			xml:      parse.XMLParser(),
			consumer: consumer,
			uc:       uc,
		}
		go ps.Serve()
	})
	return ps
}

func (p *pubsub) Serve() {
	// p.consumer.Handle("foo", p.HandleFoo)
	// p.consumer.Handle("bar", p.HandleBar)
	// p.consumer.Handle("other", p.HandleOther)
	p.consumer.Listen()
}
