package restserver

import (
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/entity"
	apperr "bitbucket.org/shipperid/sdk-go-boilerplate/src/business/errors"

	x "bitbucket.org/shipperid/sdk-go/stdlib/errors"
)

// GetBooks godoc
// @Summary Get Books
// @Description get list of book based on query params
// @Description  - Admin can get detail for every books
// @Description  - Merchant can get detail for every books
// @Tags book
// @Accept json
// @Produce json
// @Security OAuth2Application[drv]
// @Security OAuth2Password[drv]
// @Param id query []integer false "get book by IDs, support array"
// @Param title query string false "search by book title"
// @Param genre query string false "search by book genre"
// @Param author_id query int64 false "search by book's authorID"
// @Param cursor query string false "search books after start cursor"
// @Param sort_by query string false "sort result by these attributes"
// @Param page query int false " "
// @Param limit query int false " "
// @Param Cache-Control header string false "Request Cache Control" Enums(must-revalidate, none)
// @Success 200 {object} restserver.HTTPBooksResp
// @Failure 400 {object} restserver.HTTPErrResp
// @Failure 401 {object} restserver.HTTPErrResp
// @Failure 500 {object} restserver.HTTPErrResp
// @Router /book [get]
func (e *rest) GetBook(w http.ResponseWriter, r *http.Request) {
	// UNCOMMENT THIS!
	var p entity.BookParam
	log.Printf("p = %v\n", p)
	log.Printf("r.URL.Query() = %v\n", r.URL.Query())
	if err := e.param.Decode(&p, r.URL.Query()); err != nil {
		log.Println("here 1")
		e.httpRespError(w, r, x.WrapWithCode(err, apperr.CodeHTTPBadRequest, "DecodeQueryParam"))
		return
	}

	// authInfo, err := e.auth.GetAuthInfoFromContext(r.Context())
	// if err != nil {
	// 	e.httpRespError(w, r, x.WrapWithCode(err, apperr.CodeHTTPUnauthorized, "Get Auth Info"))
	// 	return
	// }

	// if authInfo.IsAdmin() {
	// 	// admin can see all drivers
	// } else if authInfo.IsAgent() {
	// 	// agent can see all drivers
	// } else if authInfo.IsDriver() {
	// 	// driver can see all drivers
	// } else if authInfo.IsMerchant() {
	// 	// merchant can see all drivers
	// }

	// var cacheControl entity.CacheControl
	// if r.Header.Get(httpheader.CacheControl) == httpheader.CacheMustRevalidate {
	// 	cacheControl.MustRevalidate = true
	// }

	// UNCOMMENT THIS
	result, pagination, err := e.uc.Book.GetBooks(r.Context(), p)
	if err != nil {
		log.Println("here 2")
		e.httpRespError(w, r, err)
		return
	}
	e.httpRespSuccess(w, r, http.StatusOK, result, &pagination)

	// var result entity.Dummy

	// result.Message = "asd"

	// e.httpRespSuccess(w, r, http.StatusOK, result, nil)
}

// GetBookByID godoc
// @Summary Get Book By Book ID
// @Description  - Admin can get detail for every books
// @Description  - Merchant can get detail for every books
// @Description return book
// @Tags book
// @Accept json
// @Produce json
// @Security OAuth2Application[drv]
// @Security OAuth2Password[drv]
// @Param id path int true "search by book ID"
// @Param Cache-Control header string false "Request Cache Control" Enums(must-revalidate, none)
// @Success 200 {object} restserver.HTTPBookResp
// @Failure 400 {object} restserver.HTTPErrResp
// @Failure 401 {object} restserver.HTTPErrResp
// @Failure 404 {object} restserver.HTTPErrResp
// @Failure 500 {object} restserver.HTTPErrResp
// @Router /book/{id} [get]
func (e *rest) GetBookByID(w http.ResponseWriter, r *http.Request) {

	idParam := r.URL.Query().Get(":id")
	_, err := regexp.MatchString("^[0-9]+$", idParam)
	if err != nil {
		e.httpRespError(w, r, x.WrapWithCode(err, apperr.CodeHTTPBadRequest, "DecodeQueryParam"))
		return
	}

	vid, err := strconv.ParseInt(idParam, 10, 64)
	if err != nil {
		e.httpRespError(w, r, x.WrapWithCode(err, apperr.CodeHTTPBadRequest, "DecodeQueryParam"))
		return
	}

	// var cacheControl entity.CacheControl
	// if r.Header.Get(httpheader.CacheControl) == httpheader.CacheMustRevalidate {
	// 	cacheControl.MustRevalidate = true
	// }

	// authInfo, err := e.auth.GetAuthInfoFromContext(r.Context())
	// if err != nil {
	// 	e.httpRespError(w, r, x.WrapWithCode(err, apperr.CodeHTTPInternalServerError, "GetAuthInfo"))
	// 	return
	// }

	// handle when ID 0 is passed
	// it will be considered as the owner wants to access his account
	// if vid == 0 {
	// 	if authInfo.IsDriver() {
	// 		// for driver only
	// 		driverInfo, err := authInfo.GetDriverInfo()
	// 		if err != nil {
	// 			e.httpRespError(w, r, x.Wrap(err, "Get Driver Info"))
	// 			return
	// 		}

	// 		vid = driverInfo.ID
	// 	} else {
	// 		// reject all token if id is 0
	// 		e.httpRespError(w, r, x.NewWithCode(apperr.CodeHTTPUnauthorized, "Only Driver is allowed to access this resource"))
	// 		return
	// 	}
	// }

	// get book by ID
	result, err := e.uc.Book.GetBookByID(r.Context(), vid)
	if err != nil {
		e.httpRespError(w, r, err)
		return
	}

	// if authInfo.IsAdmin() {
	// 	// show all records
	// } else if authInfo.IsAgent() {
	// 	// show all records
	// } else if authInfo.IsMerchant() {
	// 	// show all records
	// } else if authInfo.IsDriver() {
	// 	// if driver then check the driver account id should be match to driver account id token
	// 	driverInfo, err := authInfo.GetDriverInfo()
	// 	if err != nil {
	// 		e.httpRespError(w, r, x.Wrap(err, "Get Driver Info"))
	// 		return
	// 	}
	// 	if result.ID != driverInfo.ID {
	// 		e.httpRespError(w, r, x.NewWithCode(apperr.CodeHTTPNotFound, "ResourceNotFound"))
	// 		return
	// 	}
	// } else {
	// 	// unknown authorization token
	// 	e.httpRespError(w, r, x.NewWithCode(apperr.CodeHTTPUnauthorized, "Unknown Auth Type"))
	// 	return
	// }

	e.httpRespSuccess(w, r, http.StatusOK, result, nil)
}

// CreateBook godoc
// @Summary Add Book
// @Description Add Book
// @Description  - Admin can create a new book
// @Description  - Merchant cannot create a new book
// @Tags book
// @Accept json
// @Produce json
// @Security OAuth2Application[drv]
// @Param x-app-migration header string false "to create account with permgroupID < 3 || account type < 4 || enable auto verification, you have to supply the app migration key. Please Ask administrator to get your migration key."
// @Param x-email-sent header string false "set to true if need to skip send email delivery" Enums(true,false)
// @Param data body restserver.CreateBookRequest true "Book Data"
// @Success 201 {object} restserver.HTTPBookResp
// @Failure 400 {object} restserver.HTTPErrResp
// @Failure 409 {object} restserver.HTTPErrResp
// @Failure 429 {object} restserver.HTTPErrResp
// @Failure 500 {object} restserver.HTTPErrResp
// @Router /book [post]
func (e *rest) CreateBook(w http.ResponseWriter, r *http.Request) {
	log.Printf("r.Body = %v\n", r.Body)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		e.httpRespError(w, r, x.WrapWithCode(err, apperr.CodeHTTPBadRequest, "ParseRequestBody"))
		return
	}

	var requestBody CreateBookRequest

	log.Printf("requestBody = %v\n", requestBody)
	if err := e.json.Unmarshal(body, &requestBody); err != nil {
		e.httpRespError(w, r, x.WrapWithCode(err, apperr.CodeHTTPBadRequest, "ParseRequestBody"))
		return
	}

	log.Printf("requestBody.Data.Book = %v\n", requestBody.Data.Book)
	if requestBody.Data.Book == nil {
		e.httpRespError(w, r, x.NewWithCode(apperr.CodeHTTPBadRequest, "Invalid JSON Body"))
		return
	}

	// if requestBody.Data.Driver.MasterAccount == nil {
	// 	e.httpRespError(w, r, x.NewWithCode(apperr.CodeHTTPBadRequest, "Master Account must exist"))
	// 	return
	// }

	// authInfo, err := e.auth.GetAuthInfoFromContext(r.Context())
	// if err != nil {
	// 	e.httpRespError(w, r, x.WrapWithCode(err, apperr.CodeHTTPInternalServerError, "GetAuthInfo"))
	// 	return
	// }

	// if authInfo.IsAdmin() {
	// 	// admin role
	// } else if authInfo.IsMerchant() {
	// 	// reject merchant token and others
	// 	e.httpRespError(w, r, x.NewWithCode(apperr.CodeHTTPUnauthorized, "Try to Update Unauthorized Resource"))
	// 	return
	// }
	// else if authInfo.IsAgent() {
	// 	// agent role
	// 	// auto verification will be true if created by agent
	// 	requestBody.Data.Driver.MasterAccount.AutoVerification = true
	// }
	// }
	// else if authInfo.IsDriver() {
	// 	// driver role
	// 	// reject merchant token and others
	// 	e.httpRespError(w, r, x.NewWithCode(apperr.CodeHTTPUnauthorized, "Try to Update Unauthorized Resource"))
	// 	return
	// }
	// else {
	// 	// reject merchant token and others
	// 	e.httpRespError(w, r, x.NewWithCode(apperr.CodeHTTPUnauthorized, "Unknown Auth Type"))
	// 	return
	// }

	// checking migration key
	ctx := r.Context()
	// if requestBody.Data.Driver.MasterAccount.AccTypeID < 4 ||
	// 	requestBody.Data.Driver.MasterAccount.PermGroupID < 3 ||
	// 	requestBody.Data.Driver.MasterAccount.AutoVerification == true {
	// 	if r.Header.Get(e.opt.Driver.HeaderAppMigrationKey) != e.opt.Driver.AppMigrationKey {
	// 		e.httpRespError(w, r, x.NewWithCode(apperr.CodeHTTPUnauthorized, "Missing or Invalid Migration Key. You need Migration Key Header To Enable AutoVerification or Create Account with Admin Privilege or Enable No Hash"))
	// 		return
	// 	}

	// 	migrationKey := r.Header.Get(e.opt.Driver.HeaderAppMigrationKey)
	// 	ctx = context.WithValue(r.Context(), e.opt.Driver.HeaderAppMigrationKey, migrationKey)
	// }

	// Remap to Driver Object
	a := *requestBody.Data.Book
	// schedules := make([]entity.Schedule, 0)
	// for _, s := range a.Schedules {
	// 	schedules = append(schedules, entity.Schedule{
	// 		WindowType: s.WindowType,
	// 		TimeZone:   s.TimeZone,
	// 		Day:        s.Day,
	// 		Times:      s.Times,
	// 	})
	// }

	newBook := entity.Book{
		Title:    a.Title,
		Genre:    a.Genre,
		AuthorID: a.AuthorID,
	}

	// newDriver := entity.Driver{
	// 	Type:      a.Type,
	// 	Name:      a.MasterAccount.Name,
	// 	Phone:     a.MasterAccount.Phone,
	// 	Schedules: schedules,
	// 	Vehicle: entity.Vehicle{
	// 		Type:               a.Vehicle.Type,
	// 		Model:              a.Vehicle.Model,
	// 		Color:              a.Vehicle.Color,
	// 		PlateNumber:        a.Vehicle.PlateNumber,
	// 		PlateNumberExpDate: sqlx.NullTime{Valid: true, Time: a.Vehicle.PlateNumberExpDate},
	// 		IsActive:           true,
	// 		Ownership:          a.Vehicle.Ownership,
	// 	},
	// 	IDNumber:               a.IDNumber,
	// 	IDImageURL:             a.IDImageURL,
	// 	TaxNumber:              a.TaxNumber,
	// 	TaxImageURL:            a.TaxImageURL,
	// 	DrivingLicense:         a.DrivingLicense,
	// 	DrivingLicenseImageURL: a.DrivingLicenseImageURL,
	// 	DrivingLicenseExpDate:  sqlx.NullTime{Valid: true, Time: a.DrivingLicenseExpDate},
	// 	Status: entity.DriverStatus{
	// 		Code: a.StatusCode,
	// 	},
	// 	AgentCityIDs: a.AgentCityIDs,
	// 	IsActive:     a.IsActive,
	// 	Address:      a.Address,
	// 	Job:          a.Job,
	// 	MasterAccount: &entity.AccountDriver{
	// 		AccType: entity.AccType{
	// 			ID: a.MasterAccount.AccTypeID,
	// 		},
	// 		PermGroupID:          a.MasterAccount.PermGroupID,
	// 		Name:                 a.MasterAccount.Name,
	// 		Email:                a.MasterAccount.Email,
	// 		Phone:                a.MasterAccount.Phone,
	// 		Gender:               a.MasterAccount.Gender,
	// 		ImageURL:             a.MasterAccount.ImageURL,
	// 		Password:             a.MasterAccount.Password,
	// 		PasswordConfirmation: a.MasterAccount.PasswordConfirmation,
	// 		AutoVerification:     a.MasterAccount.AutoVerification,
	// 	},
	// }

	// noEmail := r.Header.Get("x-email-sent")
	// if noEmail == "true" {
	// 	ctx = context.WithValue(ctx, "x-email-sent", "true")
	// }

	result, err := e.uc.Book.CreateBook(ctx, newBook)
	if err != nil {
		e.httpRespError(w, r, err)
		return
	}

	e.httpRespSuccess(w, r, http.StatusCreated, result, nil)
}
