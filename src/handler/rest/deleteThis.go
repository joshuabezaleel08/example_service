package restserver

import (
	"net/http"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/entity"
)

// HelloWorld godoc
// @Summary Say hi back to world
// @Description Hello Universe
// @Tags Hello World
// @Accept json
// @Produce json
// @Success 200 {object} restserver.HTTPDummysResp
// @Router /hello [get]
func (e *rest) HelloWorld(w http.ResponseWriter, r *http.Request) {
	var result entity.Dummy

	result.Message = "Hi World!"
	result.ID = 1

	e.httpRespSuccess(w, r, http.StatusOK, result, nil)
}
