package restserver

import (
	"sync"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/usecase"
	auth "bitbucket.org/shipperid/sdk-go/stdlib/auth"
	httpmux "bitbucket.org/shipperid/sdk-go/stdlib/httpmux"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
	"bitbucket.org/shipperid/sdk-go/stdlib/parser"
)

var once = &sync.Once{}

type REST interface{}

type Options struct {
}

type rest struct {
	logger log.Logger
	opt    Options
	json   parser.JSONParser
	param  parser.ParamParser
	xml    parser.XMLParser
	mux    httpmux.HttpMux
	auth   auth.Auth
	uc     *usecase.Usecase
}

func Init(logger log.Logger, opt Options, parse parser.Parser, mux httpmux.HttpMux, auth auth.Auth, uc *usecase.Usecase) REST {
	var e *rest
	once.Do(func() {
		e = &rest{
			logger: logger,
			json:   parse.JSONParser(),
			xml:    parse.XMLParser(),
			mux:    mux,
			auth:   auth,
			uc:     uc,
		}
		e.Serve()
	})
	return e
}

func (e *rest) Serve() {
	// e.auth.AuthorizeRequest(false, "rate.shp", auth.WRITE, e.CreateOrder))
	e.mux.HandleFunc(httpmux.GET, "/v1/hello", e.HelloWorld)

	// BOOK ENDPOINT
	e.mux.HandleFunc(httpmux.GET, "/v1/book", e.GetBook)
	e.mux.HandleFunc(httpmux.GET, "/v1/book/:id", e.GetBookByID)
	e.mux.HandleFunc(httpmux.POST, "/v1/book", e.CreateBook)
	// e.mux.HandleFunc(httpmux.GET, "/v1/book",
	// 	e.auth.AuthorizeRequest(false, "book", auth.READ, e.GetBook))
	// e.mux.HandleFunc(httpmux.GET, "/v1/book/:id",
	// 	e.auth.AuthorizeRequest(false, "book", auth.READ, e.GetBookByID))
	// e.mux.HandleFunc(httpmux.POST, "/v1/book",
	// 	e.auth.AuthorizeRequest(false, "book", auth.WRITE, e.CreateBook))
	// e.mux.HandleFunc(httpmux.PUT, "/v1/book/:id",
	// 	e.auth.AuthorizeRequest(false, "book", auth.WRITE, e.UpdateBook))
	// e.mux.HandleFunc(httpmux.DELETE, "/v1/book/:id",
	// 	e.auth.AuthorizeRequest(false, "book", auth.DELETE, e.DeleteBook))

	// e.mux.HandleFunc(httpmux.PATCH, "/v1/driver/:id",
	// 	e.auth.AuthorizeRequest(false, "drv", auth.WRITE, e.PatchDriverStatus))
}
