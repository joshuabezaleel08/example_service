package restserver

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"time"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/entity"

	auth "bitbucket.org/shipperid/sdk-go/stdlib/auth"
	httpmux "bitbucket.org/shipperid/sdk-go/stdlib/httpmux"

	. "github.com/smartystreets/goconvey/convey"
)

func testGetBooksList(e *rest) {
	Convey("Success to get books list by id = 1", func() {
		var result HTTPBooksResp

		method := httpmux.GET
		path := "/v1/book"

		req, err := http.NewRequest(string(method), path, nil)
		if err != nil {
			e.logger.Fatal(err)
		}
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", authToken))

		q := req.URL.Query()
		q.Add("id", "1")
		// q.Add("status_code", "2000")
		req.URL.RawQuery = q.Encode()

		rr := httptest.NewRecorder()
		e.mux.HandleFunc(method, path,
			e.auth.AuthorizeRequest(false, `book`, auth.READ, e.GetBook))
		e.mux.Handler().ServeHTTP(rr, req)

		rawData, err := ioutil.ReadAll(rr.Body)
		if err := e.json.Unmarshal(rawData, &result); err != nil {
			e.logger.Error("Unmarshal Books List Resp ", err.Error())
		}

		bookExpRes := []entity.Book{
			{
				ID:       1,
				Title:    "BookTitle1",
				Genre:    "Genre1",
				AuthorID: 1,
				// AgentCityIDs:   []int64{2, 6, 11},
				// DriverCode:     "2004QVAV",
				// DrivingLicense: "1292921019",
				// DrivingLicenseExpDate: sqlx.NullTime{
				// 	Valid: true,
				// 	Time:  time.Date(2025, 1, 1, 0, 0, 0, 0, time.UTC),
				// },
				// IDImageURL:             "media://202004/0987654567890-098765456789",
				// DrivingLicenseImageURL: "media://202004/0987654567890-098765456789",
				// IDNumber:               "123458211",
				// IsActive:               true,
				// IsVerified:             true,
				// Name:                   "Jerome Evans",
				PartitionKey: 1,
			},
		}

		cursorStart := result.Pagination.CursorStart
		cursorEnd := result.Pagination.CursorEnd
		pagination := result.Pagination

		expectedResult := getBooksListExpectedResult(string(method), req.URL.RequestURI(), cursorStart, cursorEnd, bookExpRes, pagination)

		So(rr.Code, ShouldEqual, http.StatusOK)
		So(result, ShouldResemble, expectedResult)
	})
	// Convey("Success to get drivers list by id = 0 and status code = 2000, empty results", func() {
	// 	var result HTTPDriversResp

	// 	method := httpmux.GET
	// 	path := "/v1/driver"

	// 	req, err := http.NewRequest(string(method), path, nil)
	// 	if err != nil {
	// 		e.logger.Fatal(err)
	// 	}
	// 	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", authToken))

	// 	q := req.URL.Query()
	// 	q.Add("id", "0")
	// 	q.Add("status_code", "2000")
	// 	req.URL.RawQuery = q.Encode()

	// 	rr := httptest.NewRecorder()
	// 	e.mux.HandleFunc(method, path,
	// 		e.auth.AuthorizeRequest(false, `drv`, auth.READ, e.GetDriver))
	// 	e.mux.Handler().ServeHTTP(rr, req)

	// 	rawData, err := ioutil.ReadAll(rr.Body)
	// 	if err := e.json.Unmarshal(rawData, &result); err != nil {
	// 		e.logger.Error("Unmarshal Drivers List Resp ", err.Error())
	// 	}

	// 	driverExpRes := []entity.Driver{}

	// 	cursorStart := result.Pagination.CursorStart
	// 	cursorEnd := result.Pagination.CursorEnd
	// 	pagination := result.Pagination

	// 	expectedResult := getDriversListExpectedResult(string(method), req.URL.RequestURI(), cursorStart, cursorEnd, driverExpRes, pagination)

	// 	So(rr.Code, ShouldEqual, http.StatusOK)
	// 	So(result, ShouldResemble, expectedResult)
	// })
	// Convey("Get driver by invalid open at param, resp 400", func() {
	// 	var (
	// 		result HTTPDriversResp
	// 	)

	// 	method := httpmux.GET
	// 	path := "/v1/driver"

	// 	req, err := http.NewRequest(string(method), path, nil)
	// 	if err != nil {
	// 		e.logger.Fatal(err)
	// 	}
	// 	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", authToken))

	// 	q := req.URL.Query()
	// 	q.Add("open_at", "2020-05-05 09:00:00")
	// 	req.URL.RawQuery = q.Encode()

	// 	rr := httptest.NewRecorder()
	// 	e.mux.HandleFunc(method, path,
	// 		e.auth.AuthorizeRequest(false, `drv`, auth.READ, e.GetDriver))
	// 	e.mux.Handler().ServeHTTP(rr, req)

	// 	rawData, err := ioutil.ReadAll(rr.Body)
	// 	if err := e.json.Unmarshal(rawData, &result); err != nil {
	// 		e.logger.Error("Unmarshal Drivers List Resp ", err.Error())
	// 	}

	// 	So(rr.Code, ShouldEqual, http.StatusBadRequest)
	// })
}

func getBooksListExpectedResult(method string, path string, cursorStart *string, cursorEnd *string, books []entity.Book, pagination *entity.Pagination) HTTPBooksResp {
	return HTTPBooksResp{
		Meta: entity.Meta{
			Path:       path,
			StatusCode: 200,
			Status:     "OK",
			Message:    fmt.Sprintf("%s %s [200] OK", method, path),
			Error:      nil,
			Timestamp:  time.Now().Format(time.RFC3339),
		},
		Data: BooksData{
			books,
		},
		Pagination: pagination,
	}
}
