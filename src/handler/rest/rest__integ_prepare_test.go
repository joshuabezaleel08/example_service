package restserver

import (
	"testing"

	"bitbucket.org/shipperid/sdk-go/stdlib/httpclient"
	"github.com/golang/mock/gomock"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"

	"context"

	// Resource TCP Client Dep
	// fflag "bitbucket.org/shipperid/sdk-go/stdlib/fflag"
	// notifier "bitbucket.org/shipperid/sdk-go/stdlib/notifier"
	storage "bitbucket.org/shipperid/sdk-go/stdlib/storage"

	// Resource Storage Dep
	// cqlx "bitbucket.org/shipperid/sdk-go/stdlib/cql"
	// elastic "bitbucket.org/shipperid/sdk-go/stdlib/elastic"
	// mongo "bitbucket.org/shipperid/sdk-go/stdlib/mongo"
	// redis "bitbucket.org/shipperid/sdk-go/stdlib/redis"
	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"

	// Resource Event PubSub Dep
	kafkalib "bitbucket.org/shipperid/sdk-go/stdlib/kafkalib"
	// orch "bitbucket.org/shipperid/sdk-go/stdlib/orch"

	// Server Handlers Dep
	// "bitbucket.org/shipperid/driversvc/src/common"
	// bpmhandler "bitbucket.org/shipperid/driversvc/src/handler/bpm"
	// pubsubhandler "bitbucket.org/shipperid/driversvc/src/handler/pubsub"

	// Server Infrastructure Dep
	auth "bitbucket.org/shipperid/sdk-go/stdlib/auth"
	cfg "bitbucket.org/shipperid/sdk-go/stdlib/config"
	grace "bitbucket.org/shipperid/sdk-go/stdlib/grace"
	health "bitbucket.org/shipperid/sdk-go/stdlib/health"
	httpmiddleware "bitbucket.org/shipperid/sdk-go/stdlib/httpmiddleware"
	httpmux "bitbucket.org/shipperid/sdk-go/stdlib/httpmux"
	httpserver "bitbucket.org/shipperid/sdk-go/stdlib/httpserver"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
	parser "bitbucket.org/shipperid/sdk-go/stdlib/parser"
	telemetry "bitbucket.org/shipperid/sdk-go/stdlib/telemetry"

	// Business Layer Dep
	domain "bitbucket.org/shipperid/sdk-go-boilerplate/src/business/domain"
	usecase "bitbucket.org/shipperid/sdk-go-boilerplate/src/business/usecase"

	// mock usecase
	bookmock "bitbucket.org/shipperid/sdk-go-boilerplate/src/business/usecase/mock/book"
	// emailmock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/email"
	// locationmock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/location"
	// menumock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/menu"
	// migrationmock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/migration"
	// notificationmock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/notification"
	// phonemock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/phone"
	// pickupcustomermock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/pickupcustomer"
	// pickuprequestmock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/pickuprequest"
	// requestordermock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/requestorder"
	// routemock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/route"
	// routeoptimizationmock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/routeoptimization"
	// settingmock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/setting"
	// tripmock "bitbucket.org/shipperid/driversvc/src/business/usecase/mock/trip"
)

type Conf struct {
	// Application Metadata Options
	Meta Options
	// Resource Options TCP Client
	// FeatureFlag fflag.Options
	HTTPClient httpclient.Options
	// Notifier    notifier.Options
	Storage storage.Options
	// Resource Options Storage
	// CQL     map[string]cqlx.Options
	// Elastic map[string]elastic.Options
	// Mongo   map[string]mongo.Options
	// Redis   map[string]redis.Options
	SQL map[string]sqlx.Options
	// Resource Event Options Pubsub
	// BPM   orch.Options
	Kafka kafkalib.Options
	// Server Infrastructure Options
	RemoteKV       cfg.Options
	SecretKV       cfg.Options
	GraceApp       grace.Options
	Health         health.Options
	HTTPMux        httpmux.Options
	Auth           auth.Options
	HTTPServer     httpserver.Options
	HTTPMiddleware httpmiddleware.Options
	Log            log.Options
	Parser         parser.Options
	Telemetry      telemetry.Options

	// Handler Options
	Handler
	// Business Options
	Business
	// Business Options
	Helper

	rest REST
}

type Handler struct {
	REST Options
	// Pubsub pubsubhandler.Options
	// BPM    bpmhandler.Options
}

type Business struct {
	Domain  domain.Options
	Usecase usecase.Options
}

type Helper struct {
	// Common common.Options
}

var (
	bookUCMock *bookmock.MockUsecaseItf
)

var (
	Namespace      string
	GoVersion      string
	BuildVersion   string
	BuildTime      string
	CommitHash     string
	token          string
	authToken      string
	authTokenAdmin string
)

var (
	// Config Vars Declaration
	conf Conf

	// Resource - TCP Client
	httpClient httpclient.HTTPClient
	// featureFlag fflag.FFlag
	// notif       notifier.Notifier

	// Resource - Storage
	// redisClient0 redis.Redis
	sqlClient0 sqlx.SQL
	sqlClient1 sqlx.SQL
	// cqlClient0   cqlx.CQL

	// Resource - Event PubSub
	kafkaProd kafkalib.Producer

	// Server Infrastructure
	logger     log.Logger
	staticConf cfg.Conf
	tele       telemetry.Telemetry
	healt      health.Health
	parse      parser.Parser
	httpMware  httpmiddleware.HttpMiddleware
	httpMux    httpmux.HttpMux
	aut        auth.Auth
	httpServer httpserver.HTTPServer
	app        grace.App
	creds      oauth2.TokenSource

	// Business Layer
	dom *domain.Domain
	uc  *usecase.Usecase
)

func provideIntegrationTest(t *testing.T) func() {
	// Logger Initialization
	// Logger is initialized with default options during apps initialization
	// as it should be overridden after reading the config
	// 	output : stdout
	//	level : trace
	// 	format : txt
	logger = log.Init(log.Options{Output: "stdout", Level: "trace"})

	// Initialize Static Config
	// K8s environment mount this path as volume to get config hot reload
	// Disable the hot reload from Apps on Static Config Change, see flag : restartOnStaticConfigChange
	staticConf = cfg.Init(logger, cfg.AppStaticConfig, cfg.Options{
		// Not applicable for static configuration options
		// Provider : "",
		// Host : "",
		// Enabled : true, // always true
		// GPGKeyRing : "",
		// RemoteWatchPeriod : 0s,
		Type:            `yaml`, // always use yaml for static conf
		Path:            "./etc/conf/integ_test_rest.yaml",
		RestartOnChange: false,
	})
	staticConf.ReadAndWatch(&conf)

	// Override Logger Settings with new Logger Settings
	// Logger Default Fields is initialized
	logger.SetOptions(conf.Log)

	// Telemetry with opencensus - spawning telemetry server based on config
	// Telemetry spawns 2 server if enabled :
	// 	Stats Server ( only if you enabled Zpage Or Prometheus in the options)
	//		see HTTP Address& Port Configuration at telemetry.Exporters.Stats
	// 	Pprof Server
	//		see HTTP Address& Port Configuration at telemetry.Exporters.Profiler
	tele = telemetry.Init(logger, conf.Telemetry)

	// init Parser
	// Parser will initialize JSON& XML Parser
	parse = parser.Init(logger, conf.Parser)

	// flagr
	// featureFlag = fflag.Init(logger, tele, parse, conf.FeatureFlag)

	// Notifier initialization
	// notif = notifier.Init(logger, conf.Notifier)

	// Storage initialization
	//storag = storage.Init(logger, conf.Storage)

	// Elastic Initialization
	//elasticClient0 = elastic.Init(logger, parse, conf.Elastic["elastic-0"])

	// Redis Initialization
	// redisClient0 = redis.Init(logger, conf.Redis["redis-0"])

	// SQL initialization
	sqlClient0 = sqlx.Init(logger, conf.SQL["sql-0"])
	sqlClient1 = sqlx.Init(logger, conf.SQL["sql-1"])

	// CQL Initialization
	// cqlClient0 = cqlx.Init(log ger, conf.CQL["cql-0"])

	// HTTP Client Initialization
	httpClient = httpclient.Init(logger, tele, conf.HTTPClient)

	// Kafka Producer Initialization
	kafkaProd = kafkalib.InitProducer(logger, conf.Kafka.Producer, conf.Kafka.TraceOptions)

	// Register Liveness HealthCheck Function
	// You have to adjust your liveness check function based on your application behaviour
	// Add LivenessCheck to check the liveness as internal process monitor
	// number of goroutines, etc
	// Context will be initialized with timeout in the static config - health.liveness.CheckTimeout
	// Cancel Func must be called as deferred function to prevent context leak
	conf.Health.Liveness.CheckF = func(ctx context.Context, cancel context.CancelFunc) error {
		defer cancel()
		return nil
	}

	// Register Readiness HealthCheck Function
	// Add ReadinessCheck to SQL Database - Ping to db leader Or PingF to db followers
	// Add ReadinessCheck to Redis - Ping to redis
	// Add ReadinessCheck to check the liveness to another dependencies/ services if applicable
	// Context will be initialized with timeout in the static config - health.readiness.CheckTimeout
	// Cancel Func must be called as deferred function to prevent context leak
	conf.Health.Readiness.CheckF = func(ctx context.Context, cancel context.CancelFunc) error {
		defer cancel()
		return nil
	}

	// Healthcheck Init
	// Healthcheck will be blocking the app until the first readiness and liveness status is set
	// This behavior is intended to prevent this app to serve any kinds of TCP services so
	// The application grace upgrader won't switch before apps is ready
	// This behavior is expected to handle TCP traffic hiccup during application upgrade
	// See options : Health.waitBeforeContinue to disable this behavior
	healt = health.Init(logger, conf.Health)

	// Middleware Init
	// Default Middleware :
	//	CatchPanicAndReport - Catching Panic from any HTTP Handler and Report this to Prometheus
	// 	HealthCheck - Block any incoming traffic if the readiness and liveness check fails above threshold
	// 	RequestDump - Dump and log all incoming HTTP Requests
	// 	Secure : Block any incoming HTTP Requests that does not satisfy the Security Rules
	//		Security settings can be found in Static Options : security
	httpMware = httpmiddleware.Init(logger, healt, conf.HTTPMiddleware)

	// HTTPMux Init
	// Default Handler:
	//	ReadinessProbem default : `/ah/_health` - options : health.readiness.endpoint.
	//		OnSuccess : Return 200 - StatusOK - Body : Empty
	//		OnFailure : Return 503 - StatusServiceUnavailable - Body Unset
	//	LivenessProbe default : `/healthz` - options : health.liveness.endpoint
	//		OnSuccess : Return 200 - StatusOK - Body : Empty
	//		OnFailure : Return 503 - StatusServiceUnavailable - Body Unset
	// 	Swagger default :
	//			`/swagger/, default docFile : docs.json`
	//  Platform default :
	//			`/platform/`. Show all the default application info and static config
	//			`/platform/remote`. Show all the application remote config
	httpMux = httpmux.Init(httpmux.HTTPROUTER, logger, staticConf, staticConf, httpMware, tele, healt, conf.HTTPMux)

	// Auth Init
	aut = auth.Init(logger, conf.Auth, parse)

	// Auth Internal
	config := clientcredentials.Config{
		ClientID:     "4368c6b6-89ad-42c4-8d7b-845d0d485ad7",
		ClientSecret: "e2XXvhowNC04MlvktrhH",
		TokenURL:     "https://accountsvc.staging-0.shipper.id/v1/oauth2/token",
		Scopes:       []string{"all"},
		AuthStyle:    oauth2.AuthStyleInHeader,
	}

	creds = config.TokenSource(context.Background())

	accessToken, err := creds.Token()
	if err != nil {
		panic(err)
	}

	authToken = accessToken.AccessToken

	//common layer Initialization
	// common := common.Init(conf.Helper.Common)

	// Business layer Initialization
	dom = domain.Init(
		logger,
		conf.Business.Domain,
		parse,
		kafkaProd,
		sqlClient0,
		sqlClient1,
		httpClient,
	)

	uc = usecase.Init(logger, conf.Business.Usecase, parse, dom)

	// _ = Init(logger, conf.Handler.REST, parse, httpMux, aut, uc)

	// httpServer = httpserver.Init(logger, tele, httpMux, conf.HTTPServer)

	// app = grace.Init(logger, tele, httpServer, conf.GraceApp)

	// // start listening
	// app.Serve()

	return func() {
		if healt != nil {
			healt.Stop()
		}
		// if redisClient0 != nil {
		// 	redisClient0.Stop()
		// }
		if sqlClient0 != nil {
			sqlClient0.Stop()
		}
		if sqlClient1 != nil {
			sqlClient1.Stop()
		}
		// if cqlClient0 != nil {
		// 	cqlClient0.Stop()
		// }
		if httpClient != nil {
			httpClient.Stop()
		}
		if staticConf != nil {
			staticConf.Stop()
		}
		if logger != nil {
			logger.Stop()
		}
		// app.Stop()
	}
}

func provideIntegrationTestMock(t *testing.T) func() {
	// Logger Initialization
	// Logger is initialized with default options during apps initialization
	// as it should be overridden after reading the config
	// 	output : stdout
	//	level : trace
	// 	format : txt
	logger = log.Init(log.Options{Output: "stdout", Level: "trace"})

	// Initialize Static Config
	// K8s environment mount this path as volume to get config hot reload
	// Disable the hot reload from Apps on Static Config Change, see flag : restartOnStaticConfigChange
	staticConf = cfg.Init(logger, cfg.AppStaticConfig, cfg.Options{
		// Not applicable for static configuration options
		// Provider : "",
		// Host : "",
		// Enabled : true, // always true
		// GPGKeyRing : "",
		// RemoteWatchPeriod : 0s,
		Type:            `yaml`, // always use yaml for static conf
		Path:            "./etc/conf/integ_test_rest.yaml",
		RestartOnChange: false,
	})
	staticConf.ReadAndWatch(&conf)

	// Override Logger Settings with new Logger Settings
	// Logger Default Fields is initialized
	logger.SetOptions(conf.Log)

	// Telemetry with opencensus - spawning telemetry server based on config
	// Telemetry spawns 2 server if enabled :
	// 	Stats Server ( only if you enabled Zpage Or Prometheus in the options)
	//		see HTTP Address& Port Configuration at telemetry.Exporters.Stats
	// 	Pprof Server
	//		see HTTP Address& Port Configuration at telemetry.Exporters.Profiler
	tele = telemetry.Init(logger, conf.Telemetry)

	// init Parser
	// Parser will initialize JSON& XML Parser
	parse = parser.Init(logger, conf.Parser)

	// flagr
	// featureFlag = fflag.Init(logger, tele, parse, conf.FeatureFlag)

	// Notifier initialization
	// notif = notifier.Init(logger, conf.Notifier)

	// Storage initialization
	//storag = storage.Init(logger, conf.Storage)

	// Elastic Initialization
	//elasticClient0 = elastic.Init(logger, parse, conf.Elastic["elastic-0"])

	// Redis Initialization
	// redisClient0 = redis.Init(logger, conf.Redis["redis-0"])

	// SQL initialization
	sqlClient0 = sqlx.Init(logger, conf.SQL["sql-0"])
	sqlClient1 = sqlx.Init(logger, conf.SQL["sql-1"])

	// CQL Initialization
	// cqlClient0 = cqlx.Init(logger, conf.CQL["cql-0"])

	// HTTP Client Initialization
	httpClient = httpclient.Init(logger, tele, conf.HTTPClient)

	// Kafka Producer Initialization
	kafkaProd = kafkalib.InitProducer(logger, conf.Kafka.Producer, conf.Kafka.TraceOptions)

	// Register Liveness HealthCheck Function
	// You have to adjust your liveness check function based on your application behaviour
	// Add LivenessCheck to check the liveness as internal process monitor
	// number of goroutines, etc
	// Context will be initialized with timeout in the static config - health.liveness.CheckTimeout
	// Cancel Func must be called as deferred function to prevent context leak
	conf.Health.Liveness.CheckF = func(ctx context.Context, cancel context.CancelFunc) error {
		defer cancel()
		return nil
	}

	// Register Readiness HealthCheck Function
	// Add ReadinessCheck to SQL Database - Ping to db leader Or PingF to db followers
	// Add ReadinessCheck to Redis - Ping to redis
	// Add ReadinessCheck to check the liveness to another dependencies/ services if applicable
	// Context will be initialized with timeout in the static config - health.readiness.CheckTimeout
	// Cancel Func must be called as deferred function to prevent context leak
	conf.Health.Readiness.CheckF = func(ctx context.Context, cancel context.CancelFunc) error {
		defer cancel()
		return nil
	}

	// Healthcheck Init
	// Healthcheck will be blocking the app until the first readiness and liveness status is set
	// This behavior is intended to prevent this app to serve any kinds of TCP services so
	// The application grace upgrader won't switch before apps is ready
	// This behavior is expected to handle TCP traffic hiccup during application upgrade
	// See options : Health.waitBeforeContinue to disable this behavior
	healt = health.Init(logger, conf.Health)

	// Middleware Init
	// Default Middleware :
	//	CatchPanicAndReport - Catching Panic from any HTTP Handler and Report this to Prometheus
	// 	HealthCheck - Block any incoming traffic if the readiness and liveness check fails above threshold
	// 	RequestDump - Dump and log all incoming HTTP Requests
	// 	Secure : Block any incoming HTTP Requests that does not satisfy the Security Rules
	//		Security settings can be found in Static Options : security
	httpMware = httpmiddleware.Init(logger, healt, conf.HTTPMiddleware)

	// HTTPMux Init
	// Default Handler:
	//	ReadinessProbem default : `/ah/_health` - options : health.readiness.endpoint.
	//		OnSuccess : Return 200 - StatusOK - Body : Empty
	//		OnFailure : Return 503 - StatusServiceUnavailable - Body Unset
	//	LivenessProbe default : `/healthz` - options : health.liveness.endpoint
	//		OnSuccess : Return 200 - StatusOK - Body : Empty
	//		OnFailure : Return 503 - StatusServiceUnavailable - Body Unset
	// 	Swagger default :
	//			`/swagger/, default docFile : docs.json`
	//  Platform default :
	//			`/platform/`. Show all the default application info and static config
	//			`/platform/remote`. Show all the application remote config
	httpMux = httpmux.Init(httpmux.HTTPROUTER, logger, staticConf, staticConf, httpMware, tele, healt, conf.HTTPMux)

	// Auth Init
	aut = auth.Init(logger, conf.Auth, parse)

	// Auth Internal
	config := clientcredentials.Config{
		ClientID:     "4368c6b6-89ad-42c4-8d7b-845d0d485ad7",
		ClientSecret: "e2XXvhowNC04MlvktrhH",
		TokenURL:     "https://accountsvc.staging-0.shipper.id/v1/oauth2/token",
		Scopes:       []string{"all"},
		AuthStyle:    oauth2.AuthStyleInHeader,
	}

	creds = config.TokenSource(context.Background())

	accessToken, err := creds.Token()
	if err != nil {
		panic(err)
	}

	authToken = accessToken.AccessToken

	//common layer Initialization
	// common := common.Init(conf.Helper.Common)

	// Business layer Initialization
	dom = domain.Init(
		logger,
		conf.Business.Domain,
		parse,
		kafkaProd,
		sqlClient0,
		sqlClient1,
		httpClient,
	)

	uc = InitUseCaseMock(t)

	return func() {
		if healt != nil {
			healt.Stop()
		}
		// if redisClient0 != nil {
		// 	redisClient0.Stop()
		// }
		if sqlClient0 != nil {
			sqlClient0.Stop()
		}
		if sqlClient1 != nil {
			sqlClient1.Stop()
		}
		// if cqlClient0 != nil {
		// 	cqlClient0.Stop()
		// }
		if httpClient != nil {
			httpClient.Stop()
		}
		if staticConf != nil {
			staticConf.Stop()
		}
		if logger != nil {
			logger.Stop()
		}
	}
}

func provideREST() *rest {
	return &rest{
		logger: logger,
		opt:    conf.REST,
		json:   parse.JSONParser(),
		param:  parse.ParamParser(),
		xml:    parse.XMLParser(),
		mux:    httpMux,
		auth:   aut,
		uc:     uc,
	}
}

func InitUseCaseMock(t *testing.T) *usecase.Usecase {
	ctrl := gomock.NewController(t)
	bookUCMock = bookmock.NewMockUsecaseItf(ctrl)

	// driverUCMock = drivermock.NewMockUsecaseItf(ctrl)
	// emailUCMock = emailmock.NewMockUsecaseItf(ctrl)
	// locationUCMock = locationmock.NewMockUsecaseItf(ctrl)
	// menuUCMock = menumock.NewMockUsecaseItf(ctrl)
	// migrationUCMock = migrationmock.NewMockUsecaseItf(ctrl)
	// notificationUCMock = notificationmock.NewMockUsecaseItf(ctrl)
	// phoneUCMock = phonemock.NewMockUsecaseItf(ctrl)
	// pickupCustomerUCMock = pickupcustomermock.NewMockUsecaseItf(ctrl)
	// pickupRequestUCMock = pickuprequestmock.NewMockUsecaseItf(ctrl)
	// requestOrderUCMock = requestordermock.NewMockUsecaseItf(ctrl)
	// routeUCMock = routemock.NewMockUsecaseItf(ctrl)
	// routeOptimizationUCMock = routeoptimizationmock.NewMockUsecaseItf(ctrl)
	// settingUCMock = settingmock.NewMockUsecaseItf(ctrl)
	// tripUCMock = tripmock.NewMockUsecaseItf(ctrl)

	return &usecase.Usecase{
		Book: bookUCMock,
		// Email:             emailUCMock,
		// Location:          locationUCMock,
		// Menu:              menuUCMock,
		// Migration:         migrationUCMock,
		// Notification:      notificationUCMock,
		// Phone:             phoneUCMock,
		// PickupCustomer:    pickupCustomerUCMock,
		// PickupRequest:     pickupRequestUCMock,
		// RequestOrder:      requestOrderUCMock,
		// Route:             routeUCMock,
		// RouteOptimization: routeOptimizationUCMock,
		// Setting:           settingUCMock,
		// Trip:              tripUCMock,
	}
}
