package restserver

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestRest2(t *testing.T) {
	// close := provideIntegrationTestMock(t)
	// defer close()
	// rest := provideREST()
	// Convey("Create Request", t, func() {
	// 	testCreateRequest(rest)
	// })
}

func TestRest(t *testing.T) {
	close := provideIntegrationTest(t)
	defer close()
	rest := provideREST()

	Convey("Get Book List", t, func() {
		testGetBooksList(rest)
	})

	// Convey("Get Book By ID", t, func() {
	// 	testGetBook(rest)
	// })

	// Convey("Post Book", t, func() {
	// 	testPostBook(rest)
	// })
}
