package restserver

import "bitbucket.org/shipperid/sdk-go-boilerplate/src/business/entity"

type CreateDummyRequest struct {
	Data DummyDataRequest `json:"data"`
}

type DummyDataRequest struct {
	Dummy *entity.Dummy `json:"dummy"`
}

type CreateBookRequest struct {
	Data CreateBookData `json:"data"`
}

type CreateBookData struct {
	Book *entity.CreateBookInput `json:"book,omitempty"`
}
