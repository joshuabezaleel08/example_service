package restserver

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/entity"
	apperr "bitbucket.org/shipperid/sdk-go-boilerplate/src/business/errors"
	x "bitbucket.org/shipperid/sdk-go/stdlib/errors"
	"bitbucket.org/shipperid/sdk-go/stdlib/httpheader"
)

func (e *rest) httpRespError(w http.ResponseWriter, r *http.Request, err error) {
	debugMode := false
	lang := httpheader.LangIDN
	if r.Header.Get(httpheader.AppDebug) == "true" {
		debugMode = true
	}
	if r.Header.Get(httpheader.AppLang) == httpheader.LangEN {
		lang = httpheader.LangEN
	}

	statusCode, displayError := apperr.CompileError(err, lang, debugMode)
	statusStr := http.StatusText(statusCode)

	if statusCode == http.StatusInternalServerError {
		errCtx := r.Context().Err()
		if errCtx != nil {
			if strings.Contains(errCtx.Error(), "context canceled") {
				// 499 Client Closed Request
				statusCode = 499
				statusStr = "Client Closed Request"
			}
		}
	}

	jsonErrResp := &HTTPErrResp{
		Meta: entity.Meta{
			Path:       r.URL.String(),
			StatusCode: statusCode,
			Status:     statusStr,
			Message:    fmt.Sprintf("%s %s [%d] %s", r.Method, r.URL.RequestURI(), statusCode, statusStr),
			Error:      &displayError,
			Timestamp:  time.Now().Format(time.RFC3339),
		},
	}

	e.logger.ErrorWithContext(r.Context(), displayError.Error())

	raw, err := e.json.Marshal(jsonErrResp)
	if err != nil {
		statusCode = http.StatusInternalServerError
		e.logger.ErrorWithContext(r.Context(), err)
	}

	w.Header().Set(httpheader.ContentType, httpheader.ContentJSON)
	w.WriteHeader(statusCode)
	w.Write(raw)
	return
}

func (e *rest) httpRespSuccess(w http.ResponseWriter, r *http.Request, statusCode int, resp interface{}, p *entity.Pagination) {

	meta := entity.Meta{
		Path:       r.URL.String(),
		StatusCode: statusCode,
		Status:     http.StatusText(statusCode),
		Message:    fmt.Sprintf("%s %s [%d] %s", r.Method, r.URL.RequestURI(), statusCode, http.StatusText(statusCode)),
		Error:      nil,
		Timestamp:  time.Now().Format(time.RFC3339),
	}

	var (
		schemaName string
		raw        []byte
		err        error
	)
	schemaName = "sas"
	fmt.Println(schemaName)
	fmt.Println(meta)
	switch data := resp.(type) {
	// case entity.Orders:
	// 	schemaName = ""
	// 	orderResp := &HTTPOrdersResp{
	// 		Meta: meta,
	// 		Data: &DataOrder{
	// 			Order: &data,
	// 		},
	// 	}
	// 	raw, err = e.Marshal(schemaName, orderResp)

	case entity.Book:
		schemaName = ""
		resp := &HTTPBookResp{
			Meta: meta,
			Data: BookData{
				Book: &data,
			},
		}
		raw, err = e.Marshal(schemaName, resp)

	case entity.Dummy:
		schemaName = ""
		worldResp := &HTTPDummysResp{
			Meta: meta,
			Data: &DataDummy{
				Dummy: &data,
			},
		}
		raw, err = e.Marshal(schemaName, worldResp)
	default:
		e.httpRespError(w, r, x.NewWithCode(apperr.CodeHTTPInternalServerError, fmt.Sprintf("cannot cast type of %+v", data)))
		return
	}

	if err != nil {
		e.httpRespError(w, r, x.WrapWithCode(err, apperr.CodeHTTPInternalServerError, "MarshalHTTPResp"))
		return
	}

	w.Header().Set(httpheader.ContentType, httpheader.ContentJSON)
	w.WriteHeader(statusCode)
	w.Write(raw)
}

func (e *rest) Marshal(schema string, resp interface{}) ([]byte, error) {
	if schema != "" {
		// if err := e.json.UnmarshalWithSchemaValidation(schema, resp); err != nil {
		// 	e.httpRespError(w, r, x.WrapWithCode(err, apperr.CodeHTTPBadRequest, "ParseRequestBody"))
		// 	return
		// }
		if _, err := e.json.MarshalWithSchemaValidation(schema, &resp); err != nil {
			return nil, err
		}
	}
	return e.json.Marshal(&resp)
}
