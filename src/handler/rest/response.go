package restserver

import (
	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/entity"
)

type HTTPErrResp struct {
	Meta entity.Meta `json:"metadata"`
}

type HTTPDummysResp struct {
	Meta entity.Meta `json:"metadata"`
	Data *DataDummy  `json:"data"`
}

type DataDummy struct {
	Dummy *entity.Dummy `json:"Dummy"`
}

type HTTPEmptyResp struct {
	Meta entity.Meta `json:"metadata"`
}

type HTTPBooksResp struct {
	Meta       entity.Meta        `json:"metadata"`
	Data       BooksData          `json:"data"`
	Pagination *entity.Pagination `json:"pagination,omitempty"`
}

type HTTPBookResp struct {
	Meta entity.Meta `json:"metadata"`
	Data BookData    `json:"data"`
}

type BooksData struct {
	Books []entity.Book `json:"books"`
}

type BookData struct {
	Book *entity.Book `json:"book,omitempty"`
}
