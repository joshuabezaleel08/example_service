package bpm

import (
	"sync"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/usecase"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"

	"bitbucket.org/shipperid/sdk-go/stdlib/orch"
	"bitbucket.org/shipperid/sdk-go/stdlib/parser"
)

var once = &sync.Once{}

type BPM interface {
}

type bpm struct {
	logger   log.Logger
	opt      Options
	consumer orch.Consumer
	json     parser.JSONParser
	xml      parser.XMLParser
	uc       *usecase.Usecase
}

type Options struct{}

func Init(logger log.Logger, opt Options, consumer orch.Consumer, parse parser.Parser, uc *usecase.Usecase) BPM {
	var _bpm bpm
	if consumer == nil {
		return nil
	}
	once.Do(func() {
		_bpm := &bpm{
			logger:   logger,
			opt:      opt,
			consumer: consumer,
			json:     parse.JSONParser(),
			xml:      parse.XMLParser(),
			uc:       uc,
		}
		go _bpm.Serve()
	})
	return _bpm
}

func (b *bpm) Serve() {
	// b.consumer.Handle(`initiate-payment`, b.HandlePayment)
	b.consumer.Listen()
}
