package main

import (
	"math/rand"
	"runtime"
	"time"

	"bitbucket.org/shipperid/sdk-go-boilerplate/docs"

	cfg "bitbucket.org/shipperid/sdk-go/stdlib/config"
	"bitbucket.org/shipperid/sdk-go/stdlib/errors"
)

// @securitydefinitions.oauth2.password OAuth2Password
// @tokenUrl https://sdk-go-boilerplate.staging-0.shipper.id/v1/oauth2/token
// @scope.rate.shp Grant Shipment Location And Rate Resource Access

// @securitydefinitions.oauth2.application OAuth2Application
// @tokenUrl https://sdk-go-boilerplate.staging-0.shipper.id/v1/oauth2/token
// @scope.rate.shp Grant Shipment Location And Rate Resource Access

var (
	Namespace    string
	GoVersion    string
	BuildVersion string
	BuildTime    string
	CommitHash   string
)

const (
	DefaultMaxJitter = 2000
	DefaultMinJitter = 100
)

// SleepWithJitter will delay the app initialization between 100 ms - 2000 ms
// in order to reduce the load of service initialization
// This method can reduce the impact to the remote CMS and Service Discovery
// Consider bigger time window if there is any spike in the respective systems
func SleepWithJitter(min int, max int) {
	if min < 1 {
		min = DefaultMinJitter
	}
	if max < 1 || max < min {
		max = DefaultMaxJitter
	}
	rand.Seed(time.Now().UnixNano())
	rnd := rand.Intn(max-min) + min
	time.Sleep(time.Duration(rnd) * time.Millisecond)
}

// OverrideConfig will override the static Config as it merges the setting from static config
// and injected build variable. This will be used to update the value in the main config to be displayed
// in the platform HTTP Handler, see (/platform/)
// Swagger Config is updated here as well
func OverrideStaticConfig(staticConf cfg.Conf, conf *Conf) {

	GoVersion = runtime.Version()
	conf.Meta.Go = GoVersion
	staticConf.Set(`meta.Go`, GoVersion)
	staticConf.Set(`log.defaultfields.go`, GoVersion)

	if Namespace != "" {
		conf.Meta.Namespace = Namespace
		staticConf.Set(`meta.Namespace`, Namespace)
		staticConf.Set(`log.defaultfields.namespace`, Namespace)
	}

	if BuildVersion != "" {
		conf.Meta.Version = BuildVersion
		staticConf.Set(`meta.version`, BuildVersion)
		staticConf.Set(`log.defaultfields.version`, BuildVersion)
	}

	if BuildTime != "" {
		conf.Meta.BuildTime = BuildTime
		staticConf.Set(`meta.build`, BuildTime)
		staticConf.Set(`log.defaultfields.build`, BuildTime)
	}

	if CommitHash != "" {
		conf.Meta.CommitHash = CommitHash
		staticConf.Set(`meta.commit`, CommitHash)
		staticConf.Set(`log.defaultfields.commit`, CommitHash)
	}

	conf.Log.DefaultFields = staticConf.GetStringMapString(`log.defaultfields`)

	docs.SwaggerInfo.Title = conf.Meta.Namespace
	docs.SwaggerInfo.Description = conf.Meta.Description
	docs.SwaggerInfo.Version = conf.Meta.Version
	docs.SwaggerInfo.BasePath = conf.Meta.BasePath
	docs.SwaggerInfo.Host = conf.Meta.Host
}

func mergeConfig(dest Conf, staticConf cfg.Conf, remoteConf cfg.Conf, secretConf cfg.Conf) {
	if err := staticConf.Merge(remoteConf.AllSettings()); err != nil {
		err = errors.Wrap(err, `failed to merge remote config`)
		logger.Fatal(err)
	}

	logger.Info(`Merge Static Config With Secrets`)
	if err := staticConf.Merge(secretConf.AllSettings()); err != nil {
		err = errors.Wrap(err, `failed to merge secrets`)
		logger.Fatal(err)
	}

	if err := staticConf.Read(&conf); err != nil {
		err = errors.Wrap(err, `failed to read merged config`)
		logger.Fatal(err)
	}
}
