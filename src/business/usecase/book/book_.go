package book

import (
	"context"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/entity"

	x "bitbucket.org/shipperid/sdk-go/stdlib/errors"
	// "bitbucket.org/shipperid/sdk-go/stdlib/redis"

	// "github.com/google/uuid"
)

func (a *bookmgmt) GetBooks(ctx context.Context, p entity.BookParam) ([]entity.Book, entity.Pagination, error) {
	result, pagination, err := a.GetBooks(ctx, p)
	bookIDs := []int64{}
	for _, book := range result {
		bookIDs = append(bookIDs, book.ID)
	}

	return result, pagination, err
}

func (a *bookmgmt) GetBookByID(ctx context.Context, vid int64) (entity.Book, error) {
	result, err := a.book.GetBookByID(ctx, vid)
	return result, err
}

func (a *bookmgmt) CreateBook(ctx context.Context, v entity.Book) (entity.Book, error) {
	// _id := uuid.New().String()

	// Build partition key
	// v.PartitionKey = int64(redis.CRC16(_id)) % a.opt.PartitionSize

	v, err := a.book.CreateBook(ctx, v)
	if err != nil {
		return v, x.Wrap(err, "Create Book")
	}

	return a.GetBookByID(ctx, v.ID)
}
