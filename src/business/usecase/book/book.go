package book

import (
	"context"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/domain/book"
	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/entity"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
)

type UsecaseItf interface {
	GetBooks(ctx context.Context, p entity.BookParam) ([]entity.Book, entity.Pagination, error)
	// GetBooksWithoutPagination(ctx context.Context, c entity.CacheControl, v entity.BookParam) ([]entity.Book, entity.Pagination, error)
	GetBookByID(ctx context.Context, vid int64) (entity.Book, error)
	CreateBook(ctx context.Context, v entity.Book) (entity.Book, error)
	// UpdateBook(ctx context.Context, v entity.Book) (entity.Book, int64, error)
	// DeleteBook(ctx context.Context, vid int64) (int64, error)
}

type bookmgmt struct {
	logger log.Logger
	opt    Options

	book book.DomainItf
}

type Options struct {
	PartitionSize int64
}

func InitBookUsecase(
	logger log.Logger,
	opt Options,
	book book.DomainItf,
) UsecaseItf {
	return &bookmgmt{
		logger: logger,
		opt:    opt,
		book:   book,
	}
}
