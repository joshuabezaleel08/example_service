package book_test

import (
	"context"
	"testing"

	bookdom "bitbucket.org/shipperid/sdk-go-boilerplate/src/business/domain/book"
	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/usecase/book"

	cfg "bitbucket.org/shipperid/sdk-go/stdlib/config"
	"bitbucket.org/shipperid/sdk-go/stdlib/httpheader"

	// cqlx "bitbucket.org/shipperid/sdk-go/stdlib/cql"
	// "bitbucket.org/shipperid/sdk-go/stdlib/fflag"
	// "bitbucket.org/shipperid/sdk-go/stdlib/httpheader"
	"bitbucket.org/shipperid/sdk-go/stdlib/kafkalib"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
	"bitbucket.org/shipperid/sdk-go/stdlib/parser"

	// "bitbucket.org/shipperid/sdk-go/stdlib/redis"
	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"
	"bitbucket.org/shipperid/sdk-go/stdlib/telemetry"

	. "github.com/smartystreets/goconvey/convey"
)

var (
	logger log.Logger
	// cql    cqlx.CQL
	config cfg.Conf
	conf   Conf
	json   parser.JSONParser
	// rdis   redis.Redis
	sql sqlx.SQL
	// flag   fflag.FFlag
	kafka kafkalib.Producer
	tele  telemetry.Telemetry

	bookDom bookdom.DomainItf
	bookUC  book.UsecaseItf
)

type Conf struct {
	SQL       map[string]sqlx.Options
	Telemetry telemetry.Options
	Kafka     kafkalib.Options
}

func testMain(t *testing.T) func() {
	logger = log.Init(log.Options{})
	config = cfg.Init(logger, cfg.AppStaticConfig, cfg.Options{
		Type:            `yaml`,
		Path:            `../../../../etc/conf/integ_test.yaml`,
		RestartOnChange: false,
	})
	config.ReadAndWatch(&conf)
	// cql = cqlx.Init(logger, conf.CQL["cql-0"])
	sql = sqlx.Init(logger, conf.SQL["sql-0"])
	// rdis = redis.Init(logger, conf.Redis["redis-0"])
	json = parser.Init(logger, parser.Options{}).JSONParser()
	tele = telemetry.Init(logger, conf.Telemetry)
	// flag = fflag.Init(logger, tele, parser.Init(logger, parser.Options{}), conf.FeatureFlag)
	kafka = kafkalib.InitProducer(logger, conf.Kafka.Producer, conf.Kafka.TraceOptions)

	// ctrl := gomock.NewController(t)
	// accountDom = mock_accountdriver.NewMockDomainItf(ctrl)
	// mediaDom = mock_media.NewMockDomainItf(ctrl)

	bookDom = bookdom.InitBookDomain(logger, bookdom.Options{}, json, sql, kafka)

	bookUC = book.InitBookUsecase(logger, book.Options{}, bookDom)

	return func() {
		// cql.Stop()
		sql.Stop()
		// rdis.Stop()
		kafka.Stop()
		logger.Stop()
		config.Stop()
	}
}

func TestBook(t *testing.T) {
	t.Run("Book", func(t *testing.T) {
		close := testMain(t)
		defer close()
		Convey("Book", t, func() {
			Convey("Create Book", func() {
				sources := provideInitCreateBookData(t)
				for _, source := range sources {
					Convey(source.caseInfo.desc, func() {
						// mediaDom.EXPECT().PutFile(gomock.Any(), gomock.Any()).Return([]entity.MediaFile{}, nil)
						// accountDom.EXPECT().CreateAccountDriver(gomock.Any(), gomock.Any()).Return(entity.AccountDriver{}, nil)
						result, err := bookDom.CreateBook(context.WithValue(context.Background(), httpheader.RequestID, "req-id-driver-test-integ"), source.in)
						So(err, ShouldResemble, source.out.expError)
						So(result.Title, ShouldResemble, source.out.expOut.Title)
						// So(result.Name, ShouldResemble, source.out.expOut.Name)
						// So(result.Phone, ShouldResemble, source.out.expOut.Phone)
						// So(result.Status, ShouldResemble, source.out.expOut.Status)
						// So(len(result.Schedules), ShouldEqual, 7)
						// fill the exp out vehicle id and driver id
						// source.out.expOut.Vehicle.ID = result.Vehicle.ID
						// source.out.expOut.Vehicle.DriverID = result.ID
						// So(result.Vehicle, ShouldResemble, source.out.expOut.Vehicle)

						//delete the test data
						err = deleteBookDataTest(result.ID)
						So(err, ShouldBeNil)
					})
				}
			})

			// Convey("Update Driver", func() {
			// 	sources := provideInitCreateDriverData(t)
			// 	createdDriver, err := driverDom.CreateDriver(context.WithValue(context.Background(), httpheader.RequestID, "req-id-driver-test-integ"), sources[0].in)
			// 	So(err, ShouldBeNil)

			// 	sources = provideInitUpdateDriverData(t)
			// 	for _, source := range sources {
			// 		Convey(source.caseInfo.desc, func() {
			// 			source.in.ID = createdDriver.ID
			// 			mediaDom.EXPECT().PutFile(gomock.Any(), gomock.Any()).Return([]entity.MediaFile{}, nil)
			// 			result, _, err := driverDom.UpdateDriver(context.WithValue(context.Background(), httpheader.RequestID, "req-id-driver-test-integ"), source.in)
			// 			So(err, ShouldResemble, source.out.expError)
			// 			So(result.Name, ShouldResemble, source.out.expOut.Name)
			// 			So(result.Phone, ShouldResemble, source.out.expOut.Phone)
			// 			So(result.Status, ShouldResemble, source.out.expOut.Status)
			// 			So(len(result.Schedules), ShouldEqual, 7)
			// 			// fill the exp out vehicle id and driver id
			// 			source.out.expOut.Vehicle.ID = result.Vehicle.ID
			// 			source.out.expOut.Vehicle.DriverID = result.ID
			// 			So(result.Vehicle, ShouldResemble, source.out.expOut.Vehicle)
			// 		})
			// 	}

			// 	//delete the test data
			// 	err = deleteDriverDataTest(createdDriver.ID)
			// 	So(err, ShouldBeNil)
			// })
		})
	})
}

func deleteBookDataTest(bookID int64) error {
	_, err := sql.Leader().Exec(context.Background(), `dBookTestData`, `DELETE FROM book WHERE id = ?`, bookID)
	if err != nil {
		return err
	}

	return nil
}
