package usecase

import (
	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/domain"
	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/usecase/book"

	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
	parse "bitbucket.org/shipperid/sdk-go/stdlib/parser"
)

type Usecase struct {
	// logger log.Logger
	// option Option
	Book book.UsecaseItf
}

type Options struct {
	Book book.Options
}

func Init(
	logger log.Logger,
	option Options,
	parse parse.Parser,
	dom *domain.Domain,
) *Usecase {
	return &Usecase{
		// logger: logger,
		// option: option,
		Book: book.InitBookUsecase(
			logger,
			option.Book,
			dom.Book,
		),
	}
}
