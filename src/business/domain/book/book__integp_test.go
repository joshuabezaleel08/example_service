package book_test

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/gocql/gocql"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/entity"
	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"
)

type caseInfo struct {
	id    string
	_type string
	desc  string
}

func getInt(t *testing.T, s string) int {
	if s == "" {
		return 0
	}

	i, err := strconv.Atoi(s)
	if err != nil {
		t.Fatal(err)
	}
	return i
}

func getUint(t *testing.T, s string) uint {
	i, err := strconv.ParseUint(s, 10, 64)
	if err != nil {
		t.Fatal(err)
	}
	return uint(i)
}

func getUint8(t *testing.T, s string) uint8 {
	i, err := strconv.ParseUint(s, 10, 64)
	if err != nil {
		t.Fatal(err)
	}
	return uint8(i)
}

func getInt64(t *testing.T, s string) int64 {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		t.Fatal(err)
	}
	return i
}

func getFloat64(t *testing.T, s string) float64 {
	i, err := strconv.ParseFloat(s, 64)
	if err != nil {
		t.Fatal(err)
	}
	return i
}

func getTime(t *testing.T, s string) time.Time {
	tm, err := time.Parse(`2006-01-02 15:04:05`, s)
	if err != nil {
		t.Fatal(err)
	}
	return tm
}

func getNullableTime(t *testing.T, s string) sqlx.NullTime {
	if s == "" {
		return sqlx.NullTime{}
	}

	tm, err := time.Parse(`2006-01-02 15:04:05`, s)
	if err != nil {
		t.Fatal(err)
	}
	return sqlx.NullTime{
		Time:  tm,
		Valid: true,
	}
}

func getError(t *testing.T, s string) error {
	if s == "nil" {
		return nil
	}
	return fmt.Errorf("error")
}

func getUUID(t *testing.T, s string) gocql.UUID {
	uuid, err := gocql.ParseUUID(s)
	if err != nil {
		t.Fatal(err)
	}
	return uuid
}

func getBool(t *testing.T, s string) bool {
	if strings.ToLower(s) == "true" {
		return true
	}
	return false
}

func getTimeUUID(t *testing.T, s string) gocql.UUID {
	if s == "nil" || len(s) < 1 {
		return gocql.UUID{}
	}
	id, err := gocql.ParseUUID(s)
	if err != nil {
		t.Fatal(err)
	}
	return id
}

type outCreateBookTestCase struct {
	expOut   entity.Book
	expError error
}

type createBookTestCase struct {
	caseInfo caseInfo
	in       entity.Book
	out      outCreateBookTestCase
}

func provideInitCreateBookData(t *testing.T) (data []createBookTestCase) {
	const (
		id = iota
		testType
		desc
		partitionKey
		// driverCode
		// driverType
		// name
		// phone
		// drivingLicense
		// drivingLicenseImageURL
		// drivingLicenseExpDate
		// idNumber
		// idImageURL
		// taxNumber
		// taxImageURL
		// isActive
		// isVerified
		// status
		// agentCityIDs
		// totalPickup
		// address
		// job
		title
		genre
		authorID
		createdAt
		updatedAt
		corrKey
		expError
	)

	testFile, err := os.Open("./test_data/create_book_test_data.csv")
	if err != nil {
		t.Fatal(err)
	}
	r := csv.NewReader(testFile)

	skipHeader := true
	for {
		record, err := r.Read()
		// skip if 1
		if skipHeader {
			skipHeader = false
			continue
		}
		if err == io.EOF {
			break
		} else if err != nil {
			t.Fatal(err)
		}

		data = append(data, createBookTestCase{
			caseInfo: caseInfo{
				id:    record[id],
				_type: record[testType],
				desc:  record[desc],
			},
			in: entity.Book{
				PartitionKey: getInt64(t, record[partitionKey]),
				// DriverCode:             record[driverCode],
				// Type:                   record[driverType],
				// Name:                   record[name],
				// Phone:                  record[phone],
				// Schedules:              driverSchedule[getInt(t, record[corrKey])],
				// Vehicle:                driverVehicle[getInt(t, record[corrKey])],
				// DrivingLicense:         record[drivingLicense],
				// DrivingLicenseImageURL: record[drivingLicenseImageURL],
				// DrivingLicenseExpDate:  getNullableTime(t, record[drivingLicenseExpDate]),
				// IDNumber:               record[idNumber],
				// IDImageURL:             record[idImageURL],
				// TaxNumber:              record[taxNumber],
				// TaxImageURL:            record[taxImageURL],
				// IsActive:               getInt(t, record[isActive]) == 1,
				// IsVerified:             getInt(t, record[isVerified]) == 1,
				// Status:                 driverStatus[getInt(t, record[corrKey])],
				// AgentCityIDs:           _agentCityIDs,
				// TotalPickup:            getInt64(t, record[totalPickup]),
				// Address: sqlx.NullString{
				// 	String: record[address],
				// 	Valid:  true,
				// },
				// Job: sqlx.NullString{
				// 	String: record[job],
				// 	Valid:  true,
				// },
				// MasterAccount: nil,
				Title:     record[title],
				Genre:     record[genre],
				AuthorID:  getInt64(t, record[authorID]),
				CreatedAt: getNullableTime(t, record[createdAt]),
				UpdatedAt: getNullableTime(t, record[updatedAt]),
			},
			out: outCreateBookTestCase{
				expOut: entity.Book{
					Title: record[title],
					// Phone:     record[phone],
					// Schedules: driverSchedule[getInt(t, record[corrKey])],
					// Vehicle:   driverVehicle[getInt(t, record[corrKey])],
					// Status:    driverStatus[getInt(t, record[corrKey])],
				},
				expError: getError(t, record[expError]),
			},
		})
	}

	return data
}
