package book

import (
	"context"
	"time"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/entity"
	apperr "bitbucket.org/shipperid/sdk-go-boilerplate/src/business/errors"
	x "bitbucket.org/shipperid/sdk-go/stdlib/errors"
	qbuilder "bitbucket.org/shipperid/sdk-go/stdlib/querybuilder/sql"
	"bitbucket.org/shipperid/sdk-go/stdlib/querybuilder/utils"
	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"

	"github.com/go-sql-driver/mysql"
)

func (a *book) defaultPagination(p entity.BookParam) entity.Pagination {
	pagination := entity.Pagination{
		CurrentPage:     p.Page,
		CurrentElements: 0,
		TotalPages:      0,
		TotalElements:   0,
		SortBy:          []string{},
	}

	return pagination
}

func (a *book) getSQLBook(ctx context.Context, p entity.BookParam) ([]int64, []entity.Book, entity.Pagination, error) {
	bookIDs := make([]int64, 0)

	results := []entity.Book{}

	p.Limit = utils.ValidateLimit(p.Limit)

	p.Page = utils.ValidatePage(p.Page)

	pagination := a.defaultPagination(p)

	qb, err := qbuilder.NewSQLQueryBuilder(
		a.sql,
		"param",
		"db",
		qbuilder.SetCursor(1,
			p.Cursor,
			&entity.Book{},
			[]string{"id"}))
	if err != nil {
		return bookIDs, results, pagination, x.WrapWithCode(err, apperr.CodeSQLRead, "Get Book Query Builder")
	}

	query, args, countq, argcs, err := qb.Build(&p)
	if err != nil {
		return bookIDs, results, pagination, x.WrapWithCode(err, apperr.CodeSQLRead, "Get Book Query Builder")
	}

	// read should be from leader to maintain consistency
	a.logger.DebugWithContext(ctx, "query : ", ReadBook+query)
	a.logger.DebugWithContext(ctx, "args : ", args)
	rows, err := a.sql.Follower().Query(ctx, `rBookN`, ReadBook+query, args...)
	if err != nil {
		return bookIDs, results, pagination, x.WrapWithCode(err, apperr.CodeSQLRead, "Get Book SQL")
	}
	defer rows.Close()

	for rows.Next() {
		var result entity.Book
		if err := rows.Scan(
			&result.ID,
			&result.Title,
			&result.Genre,
			&result.AuthorID,
		); err != nil {
			return bookIDs, results, pagination, x.WrapWithCode(err, apperr.CodeSQLRowScan, "Get Books")
		}
		bookIDs = append(bookIDs, result.ID)
		results = append(results, result)
	}

	// Get Total Counts
	a.logger.DebugWithContext(ctx, "query count : ", CountBook+countq)
	a.logger.DebugWithContext(ctx, "args : ", argcs)
	var totalRecords int64
	err = a.sql.Follower().Get(ctx, `cBook`, CountBook+countq, &totalRecords, argcs...)
	if err != nil {
		return bookIDs, results, pagination, x.WrapWithCode(err, apperr.CodeSQLRowScan, "Get Books")
	}

	// Update Pagination
	totalPage := totalRecords / p.Limit
	if totalRecords%p.Limit > 0 || totalRecords == 0 {
		totalPage++
	}
	pagination.CurrentPage = p.Page
	pagination.TotalPages = totalPage
	pagination.CurrentElements = int64(len(results))
	pagination.TotalElements = totalRecords
	pagination.SortBy = p.SortBy

	if len(results) > 0 {
		cursorStart, _ := results[0].EncodeCursor()
		cursorEnd, _ := results[len(results)-1].EncodeCursor()
		pagination.CursorStart = &cursorStart
		pagination.CursorEnd = &cursorEnd
	}

	return bookIDs, results, pagination, nil
}

func (a *book) getSQLBookByID(ctx context.Context, vid int64) (entity.Book, error) {
	result := entity.Book{}

	row, err := a.sql.Leader().QueryRow(ctx, `rBook1`,
		ReadBookByID,
		vid)
	if err != nil {
		return result, x.WrapWithCode(err, apperr.CodeSQLRead, "GetBookByID")
	}

	err = row.Scan(
		&result.ID,
		&result.Title,
		&result.Genre,
		&result.AuthorID,
		&result.Author.ID,
		&result.Author.Name,
	)

	if err == sqlx.ErrNoRows {
		return result, x.WrapWithCode(err, apperr.CodeHTTPNotFound, "GetBookByID")
	} else if err != nil {
		return result, x.WrapWithCode(err, apperr.CodeSQLRowScan, "GetBookByID")
	}

	return result, nil
}

func (a *book) createSQLBook(tx sqlx.CommandTx, v entity.Book) (sqlx.CommandTx, entity.Book, error) {
	row, err := tx.Exec(`cBook1`, CreateBook,
		v.Title,
		v.Genre,
		v.AuthorID,
	)
	if err != nil {
		if mysqlError, ok := err.(*mysql.MySQLError); ok {
			// check duplicate constraint
			if mysqlError.Number == 1062 {
				return tx, v, x.WrapWithCode(err, apperr.CodeSQLUniqueConstraint, "Book Unique Constraint")
			}
		}
		return tx, v, x.WrapWithCode(err, apperr.CodeSQLCreate, "CreateBook")
	}

	// get last insert id
	v.ID, err = row.LastInsertId()
	if err != nil {
		return tx, v, x.WrapWithCode(err, apperr.CodeSQLCannotRetrieveLastInsertID, "CreateBook")
	}

	return tx, v, nil
}

func (a *book) updateSQLBook(tx sqlx.CommandTx, v entity.Book) (sqlx.CommandTx, entity.Book, int64, error) {

	// Update Book
	row, err := tx.Exec(`uBook1`, UpdateBook,
		v.Title,
		v.Genre,
		v.AuthorID,
		// where
		v.ID)
	if err != nil {
		return tx, v, 0, x.WrapWithCode(err, apperr.CodeSQLUpdate, "UpdateBook")
	}

	n, err := row.RowsAffected()
	if err != nil {
		return tx, v, 0, x.WrapWithCode(err, apperr.CodeSQLCannotRetrieveAffectedRows, "UpdateBook")
	}

	return tx, v, n, nil
}

func (a *book) deleteSQLBook(tx sqlx.CommandTx, vid int64) (sqlx.CommandTx, int64, error) {
	isDeleted := sqlx.NullBool{Valid: true, Bool: true}
	deletedAt := sqlx.NullTime{Valid: true, Time: time.Now().UTC()}
	row, err := tx.Exec(`dBook1`, DeleteBook,
		isDeleted,
		deletedAt,
		//where
		vid)
	if err != nil {
		return tx, 0, x.WrapWithCode(err, apperr.CodeSQLDelete, "DeleteBook")
	}
	n, err := row.RowsAffected()
	if err != nil {
		return tx, 0, x.WrapWithCode(err, apperr.CodeSQLCannotRetrieveAffectedRows, "DeleteBook")
	}

	return tx, n, nil
}

func (a *book) unlinkSQLBook(tx sqlx.CommandTx, vid int64) (sqlx.CommandTx, int64, error) {
	row, err := tx.Exec(`dBook1`, UnlinkBook,
		// Where
		vid)
	if err != nil {
		return tx, 0, x.WrapWithCode(err, apperr.CodeSQLUnlink, "UnlinkInternalMerchantDetail")
	}

	n, err := row.RowsAffected()
	if err != nil {
		return tx, 0, x.WrapWithCode(err, apperr.CodeSQLCannotRetrieveAffectedRows, "UnlinkInternalMerchant")
	}

	return tx, n, nil
}
