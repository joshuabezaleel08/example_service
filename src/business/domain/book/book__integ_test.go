package book_test

import (
	"context"
	"os"
	"testing"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/domain/book"

	cfg "bitbucket.org/shipperid/sdk-go/stdlib/config"
	"bitbucket.org/shipperid/sdk-go/stdlib/httpheader"
	"bitbucket.org/shipperid/sdk-go/stdlib/kafkalib"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
	"bitbucket.org/shipperid/sdk-go/stdlib/parser"
	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"
	"bitbucket.org/shipperid/sdk-go/stdlib/telemetry"
	. "github.com/smartystreets/goconvey/convey"
)

var (
	logger log.Logger
	config cfg.Conf
	conf   Conf
	json   parser.JSONParser
	sql    sqlx.SQL
	kafka  kafkalib.Producer
	tele   telemetry.Telemetry

	bookDom book.DomainItf
)

type Conf struct {
	SQL       map[string]sqlx.Options
	Telemetry telemetry.Options
	Kafka     kafkalib.Options
}

func TestMain(t *testing.M) {
	logger = log.Init(log.Options{})
	config = cfg.Init(logger, cfg.AppStaticConfig, cfg.Options{
		Type:            `yaml`,
		Path:            `../../../../etc/conf/integ_test.yaml`,
		RestartOnChange: false,
	})
	config.ReadAndWatch(&conf)
	sql = sqlx.Init(logger, conf.SQL["sql-0"])
	json = parser.Init(logger, parser.Options{}).JSONParser()
	tele = telemetry.Init(logger, conf.Telemetry)
	kafka = kafkalib.InitProducer(logger, conf.Kafka.Producer, conf.Kafka.TraceOptions)

	bookDom = book.InitBookDomain(logger, book.Options{}, json, sql, kafka)

	exitVal := t.Run()
	sql.Stop()
	kafka.Stop()
	logger.Stop()
	config.Stop()
	os.Exit(exitVal)
}

func TestBook(t *testing.T) {
	t.Run("Book", func(t *testing.T) {
		Convey("Book", t, func() {
			Convey("Create Book", func() {
				sources := provideInitCreateBookData(t)
				for _, source := range sources {
					Convey(source.caseInfo.desc, func() {
						result, err := bookDom.CreateBook(context.WithValue(context.Background(), httpheader.RequestID, "req-id-book-test-integ"), source.in)
						So(err, ShouldResemble, source.out.expError)
						So(result.Title, ShouldResemble, source.out.expOut.Title)
						// So(result.Phone, ShouldResemble, source.out.expOut.Phone)
						// So(result.Status, ShouldResemble, source.out.expOut.Status)
						// So(len(result.Schedules), ShouldEqual, 7)
						// fill the exp out vehicle id and driver id
						// source.out.expOut.Vehicle.ID = result.Vehicle.ID
						// source.out.expOut.Vehicle.DriverID = result.ID
						// So(result.Vehicle, ShouldResemble, source.out.expOut.Vehicle)

						//delete the test data
						err = deleteBookDataTest(result.ID)
						So(err, ShouldBeNil)
					})
				}
			})

			// Convey("Update Driver", func() {
			// 	sources := provideInitCreateDriverData(t)
			// 	createdDriver, err := driverDom.CreateDriver(context.WithValue(context.Background(), httpheader.RequestID, "req-id-driver-test-integ"), sources[0].in)
			// 	So(err, ShouldBeNil)

			// 	sources = provideInitUpdateDriverData(t)
			// 	for _, source := range sources {
			// 		Convey(source.caseInfo.desc, func() {
			// 			source.in.ID = createdDriver.ID
			// 			result, _, err := driverDom.UpdateDriver(context.WithValue(context.Background(), httpheader.RequestID, "req-id-driver-test-integ"), source.in)
			// 			So(err, ShouldResemble, source.out.expError)
			// 			So(result.Name, ShouldResemble, source.out.expOut.Name)
			// 			So(result.Phone, ShouldResemble, source.out.expOut.Phone)
			// 			So(result.Status, ShouldResemble, source.out.expOut.Status)
			// 			So(len(result.Schedules), ShouldEqual, 7)
			// 			// fill the exp out vehicle id and driver id
			// 			source.out.expOut.Vehicle.ID = result.Vehicle.ID
			// 			source.out.expOut.Vehicle.DriverID = result.ID
			// 			So(result.Vehicle, ShouldResemble, source.out.expOut.Vehicle)
			// 		})
			// 	}

			// 	//delete the test data
			// 	err = deleteDriverDataTest(createdDriver.ID)
			// 	So(err, ShouldBeNil)
			// })
		})
	})
}

func deleteBookDataTest(bookID int64) error {
	_, err := sql.Leader().Exec(context.Background(), `dBookTestData`, `DELETE FROM book WHERE id = ?`, bookID)
	if err != nil {
		return err
	}

	return nil
}
