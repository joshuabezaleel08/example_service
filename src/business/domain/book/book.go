package book

import (
	"context"
	"fmt"
	"regexp"
	"strconv"
	"time"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/entity"
	apperr "bitbucket.org/shipperid/sdk-go-boilerplate/src/business/errors"

	x "bitbucket.org/shipperid/sdk-go/stdlib/errors"
	"bitbucket.org/shipperid/sdk-go/stdlib/kafkalib"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
	"bitbucket.org/shipperid/sdk-go/stdlib/parser"
	"bitbucket.org/shipperid/sdk-go/stdlib/pattern"
	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"

	"github.com/nouney/randomstring"
)

type DomainItf interface {
	GetBooks(ctx context.Context, p entity.BookParam) ([]entity.Book, entity.Pagination, error)
	GetBookByID(ctx context.Context, vid int64) (entity.Book, error)
	CreateBook(ctx context.Context, v entity.Book) (entity.Book, error)
	// UpdateBook(ctx context.Context, v entity.Book) (entity.Book, int64, error)
	// DeleteBook(ctx context.Context, vid int64) (int64, error)
	// UnlinkBook(ctx context.Context, vid int64) (int64, error)
}

type Options struct{}

type book struct {
	logger log.Logger
	opt    Options
	json   parser.JSONParser
	sql    sqlx.SQL
	random *randomstring.Generator
	kafka  kafkalib.Producer
}

func InitBookDomain(logger log.Logger, opt Options, json parser.JSONParser, sql sqlx.SQL, kafka kafkalib.Producer) DomainItf {
	rand, err := randomstring.NewGenerator(randomstring.CharsetNum, randomstring.CharsetAlphaUp)
	if err != nil {
		panic(err)
	}

	return &book{
		logger: logger,
		opt:    opt,
		json:   json,
		sql:    sql,
		random: rand,
		kafka:  kafka,
	}
}

func (a *book) generateBookCode() string {
	date := time.Now().UTC()
	yearStr := strconv.Itoa(date.Year())
	return yearStr[len(yearStr)-2:] + fmt.Sprintf("%02d", int(date.Month())) + a.random.Generate(4)
}

func (a *book) setDefaultValue(v entity.Book) (entity.Book, error) {
	reg := regexp.MustCompile(pattern.NameAlphaNumericSymbol)
	if !reg.MatchString(v.Title) {
		return v, x.NewWithCode(apperr.CodeValueInvalid, "Invalid Title")
	}

	return v, nil
}
