package book

const (
	ReadBook = `SELECT
					book.id,
					book.title,
					book.genre,
					book.author_id,
					author.author_id,
					author.name,
					FROM book
					JOIN author ON book.author_id = author.id`

	CountBook = `SELECT COUNT(*) FROM book`

	ReadBookByID = `SELECT
						book.id,
						book.title,
						book.genre,
						book.author_id,
						author.id,
						author.name
					FROM book
					JOIN author ON book.author_id = author.id
					WHERE book.id = ? LIMIT 1;`

	CreateBook = `INSERT INTO book (
		title,
		genre,
		author_id
	) VALUES (?,?,?);`

	UpdateBook = `UPDATE book SET
							title = ?,
							genre = ?,
							author_id = ?,
						WHERE id = ?;`

	DeleteBook = `UPDATE book SET is_deleted = ?, deleted_at = ?
						WHERE id = ?;`

	UnlinkBook = `DELETE FROM book WHERE id = ?;`
)
