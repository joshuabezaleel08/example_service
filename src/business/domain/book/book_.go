package book

import (
	"context"
	"database/sql"

	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/entity"
	apperr "bitbucket.org/shipperid/sdk-go-boilerplate/src/business/errors"

	x "bitbucket.org/shipperid/sdk-go/stdlib/errors"
)

func (a *book) GetBooks(ctx context.Context, p entity.BookParam) ([]entity.Book, entity.Pagination, error) {
	var (
		bookIDs []int64
		err     error
	)

	result := make([]entity.Book, 0)
	pagination := entity.Pagination{}

	// Checking for particular params (?)
	// QUESTION

	bookIDs, result, pagination, err = a.getSQLBook(ctx, p)
	if err != nil {
		return result, pagination, x.Wrap(err, "Get SQL Book")
	}

	if len(bookIDs) < 1 {
		return result, a.defaultPagination(p), nil
	}

	// Adding Author Details?
	// TODO

	return result, pagination, nil
}

func (a *book) GetBookByID(ctx context.Context, vid int64) (entity.Book, error) {
	result, err := a.getSQLBookByID(ctx, vid)
	if err != nil {
		return result, err
	}

	return result, err
}

func (a *book) CreateBook(ctx context.Context, v entity.Book) (entity.Book, error) {
	// set default value
	v, err := a.setDefaultValue(v)
	if err != nil {
		return v, err
	}

	// // validate to FSM Engine (Flagr)
	// // whether the state moving is allowed
	// // 0 as new driver
	// _, err = a.validateStatusCode(ctx, 0, entity.DriverStatusLog{
	// 	Code: v.Status.Code,
	// })
	// if err != nil {
	// 	return v, err
	// }

	// v.Schedules, err = a.parseTimeToWindow(v.Schedules)
	// if err != nil {
	// 	return v, err
	// }

	// // get full status object from inserted object
	// v.Status, err = a.getSQLDriverStatusByID(ctx, v.Status.Code)
	// if err != nil {
	// 	return v, x.Wrap(err, "Create Driver")
	// }

	tx, err := a.sql.Follower().BeginTx(ctx, `txcBook1`, &sql.TxOptions{
		Isolation: sql.LevelDefault,
	})
	if err != nil {
		return v, x.WrapWithCode(err, apperr.CodeSQLTxBegin, "CreateBook")
	}

	// create book
	retry := true
	for retry {
		tx, v, err = a.createSQLBook(tx, v)
		if err != nil && x.ErrCode(err) == apperr.CodeSQLUniqueConstraint {
			// v.DriverCode = a.generateDriverCode()
			continue
		} else if err != nil {
			retry = false
			tx.Rollback()
			return v, x.Wrap(err, "CreateBook")
		}
		retry = false
	}

	if err != nil {
		tx.Rollback()
		return v, err
	}

	// tx, v.Vehicle, err = a.createSQLDriverVehicle(tx, v.ID, v.Vehicle)
	// if err != nil {
	// 	tx.Rollback()
	// 	return v, err
	// }

	// tx, v.Schedules, err = a.upsertSQLDriverSchedule(tx, v.ID, v.Schedules)
	// if err != nil {
	// 	tx.Rollback()
	// 	return v, err
	// }

	// tx, v.AgentCityIDs, err = a.createSQLDriverAgentCities(tx, v.ID, v.AgentCityIDs)
	// if err != nil {
	// 	tx.Rollback()
	// 	return v, err
	// }

	// requestID := ctx.Value(httpheader.RequestID)
	// if requestID == nil {
	// 	tx.Rollback()
	// 	return v, x.NewWithCode(apperr.CodeHTTPInternalServerError, "No Request ID")
	// }

	// reqID, ok := requestID.(string)
	// if !ok {
	// 	tx.Rollback()
	// 	return v, x.NewWithCode(apperr.CodeHTTPInternalServerError, "No Request ID")
	// }

	// driverStatusLog, err := a.validateDriverStatusLog(entity.DriverStatusLog{
	// 	ID:       fmt.Sprintf("%s-%d", reqID, time.Now().UTC().UnixNano()),
	// 	DriverID: v.ID,
	// 	Code:     v.Status.Code,
	// 	Name:     v.Status.Name,
	// })
	// if err != nil {
	// 	tx.Rollback()
	// 	return v, x.Wrap(err, "CreateDriverStatusLog")
	// }

	// // insert status log
	// tx, _, err = a.createSQLDriverStatusLog(tx, driverStatusLog)
	// if err != nil {
	// 	tx.Rollback()
	// 	return v, x.Wrap(err, "CreateDriverStatusLog")
	// }

	// Committing Invitation Creation
	if err := tx.Commit(); err != nil {
		return v, x.WrapWithCode(err, apperr.CodeSQLTxCommit, "CreateBook")
	}

	// // ignore error on persisting on cache layer
	// if _, err := a.upsertCacheDriver(ctx, v); err != nil {
	// 	a.logger.WarnWithContext(ctx, err)
	// }

	return v, nil

}
