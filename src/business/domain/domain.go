package domain

import (
	"bitbucket.org/shipperid/sdk-go-boilerplate/src/business/domain/book"

	"bitbucket.org/shipperid/sdk-go/stdlib/httpclient"
	"bitbucket.org/shipperid/sdk-go/stdlib/kafkalib"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
	parse "bitbucket.org/shipperid/sdk-go/stdlib/parser"
	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"
)

type Domain struct {
	Book book.DomainItf
}

type Options struct {
	Book book.Options
}

func Init(
	logger log.Logger,
	opt Options,
	parse parse.Parser,
	kafkaProd kafkalib.Producer,
	sql0 sqlx.SQL,
	sql1 sqlx.SQL,
	http httpclient.HTTPClient,
) *Domain {
	json := parse.JSONParser()
	// param := parse.ParamParser()
	return &Domain{
		Book: book.InitBookDomain(
			logger,
			opt.Book,
			json,
			sql0,
			kafkaProd,
		),
	}
}
