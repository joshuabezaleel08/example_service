package entity

type Dummy struct {
	ID      int64  `json:"id" bson:"id" db:"id"`
	Message string `json:"message" bson:"message" db:"message" example="Hi world!"`
}
