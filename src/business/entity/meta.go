package entity

type Meta struct {
	Path       string `json:"path"`
	StatusCode int    `json:"status_code"`
	Status     string `json:"status"`
	Message    string `json:"message"`
	Error      *error `json:"error,omitempty"`
	Timestamp  string `json:"timestamp"`
}
