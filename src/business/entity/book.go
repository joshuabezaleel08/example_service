package entity

import (
	"encoding/base64"

	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"
	"github.com/vmihailenco/msgpack"
)

// Book entity
type Book struct {
	ID           int64         `json:"id" bson:"id" db:"id"`
	PartitionKey int64         `json:"partition_key" db:"partition_key"`
	Title        string        `json:"title" bson:"title" db:"title"`
	Genre        string        `json:"genre" bson:"genre" db:"genre"`
	AuthorID     int64         `json:"author_id" bson:"author_id" db:"author_id"`
	CreatedAt    sqlx.NullTime `json:"-" db:"created_at"`
	UpdatedAt    sqlx.NullTime `json:"-" db:"updated_at"`
	Author Author `json:"author" db:""`
}

type Author struct {
	ID int64 `json:"id" bson:"id" db:"id"`
	Name string `json:"name" bson:"name" db:"name"`
}

type CreateBookInput struct {
	Title    string `json:"title"`
	Genre    string `json:"genre"`
	AuthorID int64  `json:"author_id"`
}

type UpdateBookInput struct {
	ID       int64  `json:"-"`
	Title    string `json:"title"`
	Genre    string `json:"genre"`
	AuthorID int64  `json:"author_id"`
}

type BookParam struct {
	BookID       []int64         `param:"id" json:"id" db:"book.id" fieldToLookupAtCursor:"ID"`
	Title        sqlx.NullString `param:"title" json:"title" db:"book.title" fieldToLookupAtCursor:"Title"`
	Genre        sqlx.NullString `param:"genre" json:"genre" db:"book.genre" fieldToLookupAtCursor:"Genre"`
	AuthorID     sqlx.NullInt64  `param:"author_id" json:"author_id" db:"book.author_id" fieldToLookupAtCursor:"AuthorID"`
	PartitionKey []int64         `param:"partition_key" json:"partition_key" db:"partition_key" fieldToLookupAtCursor:"PartitionKey"`
	IsDeleted    sqlx.NullBool   `param:"is_deleted" json:"is_deleted" db:"book.is_deleted" fieldToLookupAtCursor:"IsDeleted"`

	SortBy []string `param:"sort_by" db:"sort_by"`
	Page   int64    `param:"page" db:"page"`
	Cursor string   `param:"cursor" db:"-"`
	Limit  int64    `param:"limit" db:"limit"`
}

func (b *Book) DecodeCursor(_c string) error {
	raw, err := base64.StdEncoding.DecodeString(_c)
	if err != nil {
		return err
	}
	return msgpack.Unmarshal(raw, &b)
}

func (b *Book) EncodeCursor() (string, error) {
	raw, err := msgpack.Marshal(b)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(raw), nil
}
